<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Lesson;
use Illuminate\Auth\Access\HandlesAuthorization;

class LessonPolicy
{
    use HandlesAuthorization;

    public function complete(User $user, Lesson $lesson)
    {
        return $user->isPrincipal() && $lesson->canBeCompletedByPrincipal($user->principal);
    }
}
