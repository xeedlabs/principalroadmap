<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class RoadmapPolicy
{
    use HandlesAuthorization;
}
