<?php

namespace App\Policies\Admin;

use App\Models\User;
use App\Models\Lesson;
use Illuminate\Auth\Access\HandlesAuthorization;

class LessonPolicy
{
    use HandlesAuthorization;

    public function modify(User $user, Lesson $lesson)
    {
        return $user->isAdmin() || $user->isOwner($lesson->module) || $user->isOwner($lesson) || ($user->author->canModifyModule($lesson->module));
    }

    public function destroy(User $user, Lesson $lesson)
    {
        return $user->isAdmin() || $user->isOwner($lesson->module) || $user->isOwner($lesson);
    }
}
