<?php

namespace App\Policies\Admin;

use App\Models\Role;
use App\Models\User;
use App\Models\Roadmap;
use Illuminate\Auth\Access\HandlesAuthorization;

class RoadmapPolicy
{
    use HandlesAuthorization;

    public function modify(User $user, Roadmap $roadmap)
    {
        return $user->hasRole(Role::ADMIN_NAME) || $user->isOwner($roadmap);
    }
}
