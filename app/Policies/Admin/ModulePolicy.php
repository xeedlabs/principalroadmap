<?php

namespace App\Policies\Admin;

use App\Models\User;
use App\Models\Module;
use Illuminate\Auth\Access\HandlesAuthorization;

class ModulePolicy
{
    use HandlesAuthorization;

    public function modify(User $user, Module $module)
    {
        return $user->isAdmin() || $user->isOwner($module) || ($user->isAuthor() && $user->author->canModifyModule($module));
    }

    public function destroy(User $user, Module $module)
    {
        return $user->isAdmin() || $user->isOwner($module);
    }
}
