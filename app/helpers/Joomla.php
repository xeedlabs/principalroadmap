<?php

class JoomlaApp
{
    static $includes = false;
    static $user = false;

    static function includes()
    {
        if(self::$includes) return;

        define( '_JEXEC', 1 );
        define('JPATH_BASE', env('JOOMLA_PATH'));
        define( 'DS', DIRECTORY_SEPARATOR );

        require_once (JPATH_BASE . DS . 'includes' . DS . 'defines.php');
        require_once (JPATH_BASE . DS . 'includes' . DS . 'framework.php');
        $mainframe = JFactory::getApplication('site');
        self::$includes = true;
    }

    static function getUser()
    {
        if (self::$user) {
            return self::$user;
        }

        self::includes();

        $user = JFactory::getUser();

        if($user->get('id'))
        {
            self::$user = new stdClass;
            self::$user->id = $user->get('id');
            self::$user->email = $user->get('email');
            self::$user->name = $user->get('name');
            self::$user->groups = $user->getAuthorisedGroups();
            return self::$user;
        }
        return false;
    }
}
