<?php

namespace App\Services;

class URLParser
{
    public static function parse($url)
    {
        return parse_url($url);
    }

    public static function getParameters($url)
    {
        $parsed       = self::parse($url);
        $query_string = empty($parsed['query']) ? '' : $parsed['query'];
        $parameters   = [];
        parse_str($query_string, $parameters);

        return $parameters;
    }

    public static function getParameter($url, $name)
    {
        return preg_match("/{$name}=(\d+)/", $url, $matches) ? $matches[1] : null;
        /*
        $parameters = self::getParameters($url);

        return empty($parameters[$name])? null: $parameters[$name];
        */
    }
}
