<?php

namespace App\Providers;

use App\Models\Roadmap;
use App\Models\User;
use App\Models\Lesson;
use App\Models\Module;
use App\Models\ModuleRoadmap;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher $events
     *
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        User::creating(function ($model) {
            $this->assignUuid($model);
        });

        Lesson::saving(function ($model) {
            $this->setOrder($model->id, $model->module_id, Module::class, 'lessons', $model->order);
        });

        ModuleRoadmap::saving(function ($model) {
            $this->setOrder($model->id, $model->roadmap_id, Roadmap::class, 'pivotModules', $model->order,
                'module_roadmap');
        });
    }

    /**
     * Generates and uuid and sets it to a model if none was set previously
     *
     * @param Model $model
     *
     * @throws \Exception
     */
    protected function assignUuid(Model $model)
    {
        $uuid = \Uuid::generate();
        if ($model->uuid || $uuid) {
            $model->uuid = $model->uuid ?: $uuid;
        }
    }

    /**
     * Sets the order in which a module or lesson should be taken
     *
     * @param $id
     * @param $parent_id
     * @param $class
     * @param $children
     * @param $order
     */
    protected function setOrder($id, $parent_id, $class, $children, $order, $model_table = '')
    {
        $parent = $class::find($parent_id);

        $query = $parent->$children()->whereOrder($order)->orderBy('order');
        if ($id) {
            $model_id_field = $model_table ? $model_table . '.id' : 'id';
            $query->where($model_id_field, '!=', $id);
        }

        foreach ($query->get() as $model) {
            if ($order == $model->order) {
                $model->order++;
                $model->save();
            } else {
                break;
            }
        }
    }
}
