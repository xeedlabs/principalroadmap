<?php

namespace App\Providers;

use Validator;
use App\Models\User;
use App\Models\Module;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('exists_slugs', function ($attribute, $values, $parameters) {
            $valid = false;

            if (is_array($values)) {
                $class = '\\App\\Models\\' . ucfirst($parameters[0]);
                $count = $class::whereIn('slug', $values)->count();
                $valid = count($values) == $count;
            }

            return $valid;
        });

        Validator::extend('exists_ids', function ($attribute, $values, $parameters) {
            $valid = false;

            if (is_array($values)) {
                $class = '\\App\\Models\\' . ucfirst($parameters[0]);
                $count = $class::whereIn('id', $values)->count();
                $valid = count($values) == $count;
            }

            return $valid;
        });

        Validator::extend('exists_uuid', function ($attribute, $value, $parameters) {
            $valid = false;

            if (($user = User::whereUuid($value)->first())) {
                $relation = $parameters[0];
                $valid    = !!$user->$relation()->count();
            }

            return $valid;
        });

        Validator::extend('exists_uuids', function ($attribute, $values, $parameters) {
            $valid = false;

            if (is_array($values)) {
                $users_count = User::join($parameters[0], 'users.id', '=', "{$parameters[0]}.id")->whereIn('uuid',
                    $values)->count();
                $valid       = count($values) == $users_count;
            }

            return $valid;
        });

        Validator::extend('required_if_not_author', function ($attribute, $value, $parameters) {
            return !$value && !(\Auth::user() && \Auth::user()->author);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
