<?php

namespace App\Providers;

use App\Models\Lesson;
use App\Models\Module;
use App\Models\Roadmap;
use App\Models\Permission;
use App\Policies\LessonPolicy;
use App\Policies\ModulePolicy;
use App\Policies\RoadmapPolicy;
use App\Policies\Admin\LessonPolicy as AdminLessonPolicy;
use App\Policies\Admin\ModulePolicy as AdminModulePolicy;
use App\Policies\Admin\RoadmapPolicy as AdminRoadmapPolicy;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
    ];

    public function __construct($app)
    {
        if (strpos(strtolower(\Request::server('REQUEST_URI')), '/admin') === 0) {
            $this->policies = [
                Lesson::class  => AdminLessonPolicy::class,
                Module::class  => AdminModulePolicy::class,
                Roadmap::class => AdminRoadmapPolicy::class,
            ];
        } else {
            $this->policies = [
                Lesson::class  => LessonPolicy::class,
                Module::class  => ModulePolicy::class,
                Roadmap::class => RoadmapPolicy::class,
            ];
        }

        parent::__construct($app);
    }

    /**
     * Register any application authentication / authorization services.
     *
     * @param GateContract $gate
     *
     * @return array
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);
        if (app()->runningInConsole()) {
            return [];
        }
        foreach (Permission::with('roles')->get() as $permission) {
            $gate->define($permission->name, function ($user) use ($permission) {
                return $user->hasRoles($permission->roles);
            });
        }
    }
}
