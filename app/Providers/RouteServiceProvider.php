<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $uuid_pattern = '[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}';

        $router->pattern('user', $uuid_pattern);

        $router->bind('users', function ($user) {
            return User::whereUuid($user)->firstOrFail();
        });

        $router->bind('admins', function ($admin) {
            return User::whereUuid($admin)->firstOrFail()->admin()->firstOrFail();
        });

        $router->bind('authors', function ($author) {
            return User::whereUuid($author)->firstOrFail()->author()->firstOrFail();
        });

        $router->bind('principals', function ($principal) {
            return User::whereUuid($principal)->firstOrFail()->principal()->firstOrFail();
        });

        $router->bind('roadmaps', function ($slug) {
            return \App\Models\Roadmap::whereSlug($slug)->firstOrFail();
        });

        $router->bind('modules', function ($slug) {
            return \App\Models\Module::whereSlug($slug)->firstOrFail();
        });

        $router->bind('lessons', function ($slug) {
            return \App\Models\Lesson::whereSlug($slug)->firstOrFail();
        });

        $router->bind('resources', function ($slug) {
            return \App\Models\Resource::whereSlug($slug)->firstOrFail();
        });

        $router->bind('tags', function ($slug) {
            return \App\Models\Tag::whereSlug($slug)->firstOrFail();
        });

        $router->bind('roles', function ($name) {
            return \App\Models\Role::whereName($name)->firstOrFail();
        });

        $router->bind('permissions', function ($name) {
            return \App\Models\Permission::whereSlug($name)->firstOrFail();
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
