<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        Middleware\VerifyCsrfToken::class,
        Middleware\FilterIfPjax::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth'                 => Middleware\Authenticate::class,
        'auth.basic'           => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest'                => Middleware\RedirectIfAuthenticated::class,
        'ACL'                  => Middleware\Authorize::class,
        'admin.module.modify'  => Middleware\DeniesIfCantModifyModule::class,
        'admin.module.destroy' => Middleware\DeniesIfCantDestroyModule::class,
        'admin.lesson.modify'  => Middleware\DeniesIfCantModifyLesson::class,
        'admin.lesson.destroy' => Middleware\DeniesIfCantDestroyLesson::class,
        'admin.auth'           => Middleware\AdminAuth::class,
        'app.auth'             => Middleware\AppAuth::class,
        'auth.joomla'          => Middleware\AuthJoomla::class,
    ];
}
