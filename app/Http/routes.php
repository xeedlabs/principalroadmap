<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group([
    //'domain'     => Config::get('app.domain'),
    'namespace'  => 'Admin',
    'as'         => 'admin.',
    'middleware' => [
        'auth.joomla',
        //'admin.auth',
        'ACL',
    ],
    'prefix'     => 'admin'
], function () {
    Route::resource('roadmaps', 'RoadmapsController', [
        'names' => [
            'index'   => 'roadmaps.index',
            'create'  => 'roadmaps.create',
            'store'   => 'roadmaps.store',
            'show'    => 'roadmaps.show',
            'edit'    => 'roadmaps.edit',
            'update'  => 'roadmaps.update',
            'destroy' => 'roadmaps.destroy',
        ]
    ]);

    Route::group(['prefix' => 'roadmaps', 'as' => 'roadmaps.'], function () {
        Route::resource('{roadmaps}/modules', 'ModuleRoadmapController', [
            'except' => ['show', 'create', 'store'],
            'names'  => [
                'index'   => 'modules.index',
                'edit'    => 'modules.edit',
                'update'  => 'modules.update',
                'destroy' => 'modules.destroy',
            ]
        ]);
    });

    Route::resource('modules', 'ModulesController', [
        'only'  => ['index', 'show', 'create', 'store'],
        'names' => [
            'index'  => 'modules.index',
            'show'   => 'modules.show',
            'create' => 'modules.create',
            'store'  => 'modules.store',
        ],
    ]);

    Route::group(['middleware' => ['admin.module.modify']], function () {
        Route::resource('modules', 'ModulesController', [
            'only'  => ['edit', 'update'],
            'names' => [
                'edit'   => 'modules.edit',
                'update' => 'modules.update',
            ],
        ]);
    });

    Route::delete('modules/{modules}', [
        'middleware' => ['admin.module.destroy'],
        'as'         => 'modules.destroy',
        'uses'       => 'ModulesController@destroy'
    ]);

    Route::group(['prefix' => 'modules', 'as' => 'modules.'], function () {
        Route::group(['middleware' => ['admin.module.modify']], function () {
            Route::resource('{modules}/lessons', 'LessonModuleController', [
                'only'  => ['create', 'store'],
                'names' => [
                    'create' => 'lessons.create',
                    'store'  => 'lessons.store',
                ],
            ]);
        });

        Route::group(['middleware' => ['admin.lesson.modify']], function () {
            Route::resource('{modules}/lessons', 'LessonModuleController', [
                'only'  => ['edit', 'update'],
                'names' => [
                    'edit'   => 'lessons.edit',
                    'update' => 'lessons.update',
                ],
            ]);
        });

        Route::resource('{modules}/lessons', 'LessonModuleController', [
            'only'  => ['index', 'show'],
            'names' => [
                'index' => 'lessons.index',
                'show'  => 'lessons.show',
            ],
        ]);

        Route::group(['middleware' => ['admin.lesson.destroy']], function () {
            Route::resource('{modules}/lessons', 'LessonModuleController', [
                'only'  => ['destroy'],
                'names' => [
                    'destroy' => 'lessons.destroy',
                ],
            ]);
        });
    });

    Route::resource('lessons', 'LessonsController', [
        'only'  => ['index', 'show', 'create', 'store'],
        'names' => [
            'index'  => 'lessons.index',
            'show'   => 'lessons.show',
            'create' => 'lessons.create',
            'store'  => 'lessons.store',
        ],
    ]);

    Route::group(['middleware' => ['admin.lesson.modify']], function () {
        Route::resource('lessons', 'LessonsController', [
            'only'  => ['edit', 'update'],
            'names' => [
                'edit'   => 'lessons.edit',
                'update' => 'lessons.update',
            ],
        ]);
    });

    Route::delete('lessons/{lessons}', [
        'middleware' => ['admin.lesson.destroy'],
        'as'         => 'lessons.destroy',
        'uses'       => 'LessonsController@destroy'
    ]);

    Route::resource('resources', 'ResourcesController', [
        'names' => [
            'index'   => 'resources.index',
            'create'  => 'resources.create',
            'store'   => 'resources.store',
            'show'    => 'resources.show',
            'edit'    => 'resources.edit',
            'update'  => 'resources.update',
            'destroy' => 'resources.destroy',
        ]
    ]);
    Route::post('resources/upload', [
        'as'   => 'resources.upload',
        'uses' => 'ResourcesController@upload'
    ]);

    Route::resource('tags', 'TagsController', [
        'names' => [
            'index'   => 'tags.index',
            'create'  => 'tags.create',
            'store'   => 'tags.store',
            'show'    => 'tags.show',
            'edit'    => 'tags.edit',
            'update'  => 'tags.update',
            'destroy' => 'tags.destroy',
        ]
    ]);

    Route::get('/', ['as' => 'home.index', 'uses' => 'HomeController@index']);
});

Route::group([
    'middleware' => [
        //'auth.joomla',
        //'app.auth',
        //'ACL'
    ],
], function () {
    Route::get('', [
        'uses' => 'RoadmapsController@index'
    ]);

    Route::resource('roadmaps', 'RoadmapsController', [
        'only'  => ['index', 'show'],
        'names' => [
            'index' => 'roadmaps.index',
            'show'  => 'roadmaps.show',
        ]
    ]);
    Route::group(['prefix' => 'roadmaps', 'as' => 'roadmaps.'], function () {
        Route::resource('{roadmaps}/lessons', 'Roadmaps\LessonsController', [
            'only'  => ['index', 'show'],
            'names' => [
                'index' => 'lessons.index',
                'show'  => 'lessons.show',
            ]
        ]);

        Route::post('{roadmaps}/lessons/{lessons}/complete',
            ['as' => 'lessons.complete', 'uses' => 'Roadmaps\LessonsController@complete']);
        Route::post('{roadmaps}/lessons/{lessons}/share',
            ['as' => 'lessons.share', 'uses' => 'Roadmaps\LessonsController@share']);
    });

    Route::resource('tags', 'TagsController', [
        'only'  => ['index', 'show'],
        'names' => [
            'index' => 'tags.index',
            'show'  => 'tags.show',
        ]
    ]);

    Route::resource('modules', 'ModulesController', [
        'only'  => ['index'],
        'names' => [
            'index' => 'modules.index'
        ]
    ]);
    Route::resource('lessons', 'LessonsController', [
        'only'  => ['index', 'show'],
        'names' => [
            'index' => 'lessons.index',
            'show' => 'lessons.show'
        ]
    ]);
    Route::resource('resources', 'ResourcesController', [
        'only'  => ['index'],
        'names' => [
            'index' => 'resources.index'
        ]
    ]);
});
