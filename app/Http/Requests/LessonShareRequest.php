<?php

namespace App\Http\Requests;

class LessonShareRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return self::baseRules();
    }

    public static function baseRules()
    {
        return [
            'email'   => 'required|email',
            'name'    => 'required|min:3',
            'message' => 'required|min:3',
        ];
    }
}
