<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class LessonRequest extends Request
{
    public function attributes()
    {
        return self::baseAttributes();
    }

    public static function baseAttributes()
    {
        return [
            'title'          => trans('lesson.fields.title'),
            'module'         => trans('lesson.fields.module'),
            'image_url'      => trans('lesson.fields.image_url'),
            'description'    => trans('lesson.fields.description'),
            'bonus'          => trans('lesson.fields.bonus'),
            'order'          => trans('lesson.fields.order'),
            'tags'           => trans('lesson.fields.tags'),
            'resources'      => trans('lesson.fields.resources'),
            'resource-files' => trans('lesson.fields.resource-files'),
            'ect'            => trans('lesson.fields.ect'),
        ];
    }
}
