<?php

namespace App\Http\Requests\Admin;

class ModuleStoreRequest extends ModuleRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return self::baseRules();
    }

    public static function baseRules()
    {
        $authors_required = '';

        if (!(\Auth::user() && \Auth::user()->isAuthor())) {
            $authors_required = 'required|';
        }

        return [
            'author'  => "{$authors_required}exists_uuid:author",
            'authors' => "exists_uuids:authors",
            'title'   => 'required|max:255',
        ];
    }
}
