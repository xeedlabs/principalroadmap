<?php

namespace App\Http\Requests\Admin;

class TagStoreRequest extends TagRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return self::baseRules();
    }

    public static function baseRules()
    {
        return [
            'name' => 'required|max:255',
        ];
    }
}
