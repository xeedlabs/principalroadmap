<?php

namespace App\Http\Requests\Admin;

class ModuleUpdateRequest extends ModuleRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return self::baseRules();
    }

    public static function baseRules()
    {
        $create_rules = ModuleStoreRequest::baseRules();

        $rules = [];

        return array_merge($create_rules, $rules);
    }
}
