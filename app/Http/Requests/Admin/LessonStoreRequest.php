<?php

namespace App\Http\Requests\Admin;

use App\Models\Lesson;

class LessonStoreRequest extends LessonRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return self::baseRules();
    }

    public static function baseRules()
    {
        $authors_required = '';

        if (!(\Auth::user() && \Auth::user()->isAuthor())) {
            $authors_required = 'required|';
        }

        $types = implode(',', Lesson::types());

        return [
            'author'    => "{$authors_required}exists_uuid:author",
            'module'    => 'required|exists:modules,slug',
            'title'     => 'required|max:255',
            'order'     => 'required|integer|min:1',
            'tags'      => 'exists_slugs:tag',
            'resources' => 'exists_slugs:resource',
            'type'      => "in:{$types}",
            'ect'       => 'required|min:1|max:1000'
        ];
    }
}
