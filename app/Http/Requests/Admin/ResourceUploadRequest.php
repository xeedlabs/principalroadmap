<?php

namespace App\Http\Requests\Admin;

class ResourceUploadRequest extends ResourceRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return self::baseRules();
    }

    public static function baseRules()
    {
        return [
            'resource-file' => 'mimes:jpeg,bmp,png,jpg,pdf,rar,zip,epub|max:10240', //10mb
        ];
    }
}
