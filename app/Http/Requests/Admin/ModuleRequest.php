<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class ModuleRequest extends Request
{
    public function attributes()
    {
        return self::baseAttributes();
    }

    public static function baseAttributes()
    {
        return [
            'author'      => trans('module.fields.author'),
            'authors'     => trans('module.fields.authors'),
            'title'       => trans('module.fields.title'),
            'image_url'   => trans('module.fields.image_url'),
            'description' => trans('module.fields.description'),
        ];
    }
}
