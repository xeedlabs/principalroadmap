<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class RoadmapRequest extends Request
{
    public function attributes()
    {
        return self::baseAttributes();
    }

    public static function baseAttributes()
    {
        return [
            'principal'   => trans('roadmap.fields.principal'),
            'title'       => trans('roadmap.fields.title'),
            'description' => trans('roadmap.fields.description'),
            'image_url'   => trans('roadmap.fields.image_url'),
            'modules'     => trans('roadmap.fields.modules'),
            'groups'     => trans('roadmap.fields.groups'),
        ];
    }
}
