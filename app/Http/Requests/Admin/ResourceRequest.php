<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class ResourceRequest extends Request
{
    public function attributes()
    {
        return self::baseAttributes();
    }

    public static function baseAttributes()
    {
        return [
            'title'       => trans('resource.fields.title'),
            'url'         => trans('resource.fields.image_url'),
            'description' => trans('resource.fields.description'),
        ];
    }
}
