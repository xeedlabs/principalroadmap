<?php

namespace App\Http\Requests\Admin;

class ModuleRoadmapUpdateRequest extends ModuleRoadmapRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return self::baseRules();
    }

    public static function baseRules()
    {
        $create_rules = ModuleRoadmapStoreRequest::baseRules();

        $rules = [];

        return array_merge($create_rules, $rules);
    }
}
