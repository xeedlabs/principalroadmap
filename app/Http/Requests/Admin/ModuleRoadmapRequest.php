<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class ModuleRoadmapRequest extends Request
{
    public function attributes()
    {
        return self::baseAttributes();
    }

    public static function baseAttributes()
    {
        return [
            'bonus' => trans('roadmap.module.fields.bonus'),
            'order' => trans('roadmap.module.fields.order'),
        ];
    }
}
