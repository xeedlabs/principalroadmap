<?php

namespace App\Http\Requests\Admin;

class ResourceStoreRequest extends ResourceRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return self::baseRules();
    }

    public static function baseRules()
    {
        return [
            'title' => 'required|max:255',
            'url'   => 'required'
        ];
    }
}
