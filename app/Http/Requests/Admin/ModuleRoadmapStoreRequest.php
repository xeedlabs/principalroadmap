<?php

namespace App\Http\Requests\Admin;

class ModuleRoadmapStoreRequest extends ModuleRoadmapRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return self::baseRules();
    }

    public static function baseRules()
    {
        return [
            'order' => 'required|integer|min:1'
        ];
    }
}
