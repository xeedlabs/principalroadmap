<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class TagRequest extends Request
{
    public function attributes()
    {
        return self::baseAttributes();
    }

    public static function baseAttributes()
    {
        return [
            'name'        => trans('tag.fields.name'),
            'description' => trans('tag.fields.description'),
        ];
    }
}
