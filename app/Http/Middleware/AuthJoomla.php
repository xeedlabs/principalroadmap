<?php

namespace App\Http\Middleware;

use App\Models\Admin;
use App\Models\Author;
use App\Models\Group;
use App\Models\Principal;
use App\Models\Role;
use App\Models\User;
use Closure;
use Illuminate\Contracts\Auth\Guard;

class AuthJoomla
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!($joomla_user_info = \JoomlaApp::getUser()))
        {
            $return = isset($_SERVER['REQUEST_URI']) ? '&ret='.urlencode($_SERVER['REQUEST_URI']) : '';
            return redirect(env('JOOMLA_LOGIN').$return);
        }

        $this->syncUser($joomla_user_info);

        return $next($request);
    }

    protected function syncUser(\StdClass $joomla_user_info)
    {
        if (($user = User::whereEmail($joomla_user_info->email)->first()) && $user->id != $joomla_user_info->id) {
            $user->email = $user->id;
            $user->save();
            // TODO:: improve control on repeated emails
        }

        $user = User::findOrNew($joomla_user_info->id);
        $user->id = $joomla_user_info->id;
        $user->first_name = $joomla_user_info->name;
        $user->email = $joomla_user_info->email;
        $user->save();

        $available_groups = Group::whereIn('id', $joomla_user_info->groups)->get();

        $user->groups()->sync($available_groups);

        foreach ($user->groups as $group) {
            switch ($group->id) {
                case Group::ADMINS_AUTHORS:
                    $user->addToRole(Role::ADMIN_NAME);
                    Admin::firstOrCreate(['id' => $user->id]);
                    //break;

                case Group::AUTHORS:
                    $user->addToRole(Role::AUTHOR_NAME);
                    Author::firstOrCreate(['id' => $user->id]);
                    break;

                case Group::PRINCIPALS_K_5;
                case Group::PRINCIPALS_6_12;
                case Group::PRINCIPALS_K_12;
                    $user->addToRole(Role::PRINCIPAL_NAME);
                    Principal::firstOrCreate(['id' => $user->id]);
                    break;
            }
        }

        \Auth::login($user);
    }
}
