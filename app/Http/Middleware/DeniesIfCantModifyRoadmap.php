<?php

namespace App\Http\Middleware;

use Closure;

class DeniesIfCantModifyRoadmap
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $roadmap = $request->roadmaps;

        if (\Gate::denies('modify', $roadmap)) {
            abort(403, trans('acl.errors.forbidden'));
        }

        return $next($request);
    }
}
