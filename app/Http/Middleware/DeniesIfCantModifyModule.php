<?php

namespace App\Http\Middleware;

use Closure;

class DeniesIfCantModifyModule
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $module = $request->modules;

        if (\Gate::denies('modify', $module)) {
            abort(403, trans('acl.errors.forbidden'));
        }

        return $next($request);
    }
}
