<?php

namespace App\Http\Middleware;

use Closure;

class DeniesIfCantModifyLesson
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lesson = $request->lessons;

        if (\Gate::denies('modify', $lesson)) {
            abort(403, trans('acl.errors.forbidden'));
        }

        return $next($request);
    }
}
