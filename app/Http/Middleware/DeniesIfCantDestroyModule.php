<?php

namespace App\Http\Middleware;

use Closure;

class DeniesIfCantDestroyModule
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $module = $request->modules;

        if (\Gate::denies('destroy', $module)) {
            abort(403, trans('acl.errors.forbidden'));
        }

        return $next($request);
    }
}
