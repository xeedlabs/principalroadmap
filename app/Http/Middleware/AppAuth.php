<?php

namespace App\Http\Middleware;

use Closure;

class AppAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Auth::loginUsingId(10);
        return $next($request);
    }
}
