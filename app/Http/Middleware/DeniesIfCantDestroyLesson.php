<?php

namespace App\Http\Middleware;

use Closure;

class DeniesIfCantDestroyLesson
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lessons = $request->lessons;

        if (\Gate::denies('destroy', $lessons)) {
            abort(403, trans('acl.errors.forbidden'));
        }

        return $next($request);
    }
}
