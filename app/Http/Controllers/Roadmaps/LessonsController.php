<?php

namespace App\Http\Controllers\Roadmaps;

use App\AjaxResponse;
use App\Models\Lesson;
use App\Models\Roadmap;
use Illuminate\Mail\Message;
use App\Http\Controllers\Controller;
use App\Http\Requests\LessonShareRequest;

class LessonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'Lessons list';
    }

    /**
     * Display the specified resource.
     *
     * @param Roadmap $roadmap
     * @param Lesson  $lesson
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function show(Roadmap $roadmap, Lesson $lesson)
    {
        return \View::make('roadmaps.lessons.show', compact('roadmap', 'lesson'));
    }

    public function complete(Roadmap $roadmap, Lesson $lesson)
    {
        $response          = new AjaxResponse();
        $response->success = $lesson->addPrincipal(\Auth::user()->principal);

        return $response;
    }

    public function share(LessonShareRequest $request, Roadmap $roadmap, Lesson $lesson )
    {
        $response = new AjaxResponse();

        $name    = $request->get('name', 'User');
        $email   = $request->get('email', 'user@viflearn.com');
        $message = $request->get('message', '');

        if (null === ($user = \Auth::user())) {
            $user       = new \stdClass();
            $user->name = 'Someone';
        }

        $data = [
            'user'    => $user,
            'lesson'  => $lesson,
            'roadmap' => $roadmap,
            'name'    => $name,
            'email'   => $email,
            'content' => $message,
        ];

        try {
            $sent = \Mail::send('emails.lessons.share', $data, function (Message $mail) use ($user, $name, $email) {
                if (!empty($user->email)) {
                    $mail->from($user->email);
                }

                $mail->to($email, $name);
                $mail->subject('Check this out on the VIF learning center!');
            });

            $response->success = (bool) $sent;
        } catch (\Exception $e) {
            $response->addError('Mail couldn\'t be sent.');
        }

        return $response;
    }
}
