<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Lesson;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::with(['lessons', 'lessons.module', 'lessons.module.roadmaps'])->get();

        return \View::make('tags.index', compact('tags'));
    }

    /**
     * Display the specified resource.
     *
     * @param  Tag $tag
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        $tag = Tag::with(['lessons', 'lessons.module', 'lessons.module.roadmaps'])->find($tag->id);

        return \View::make('tags.show', compact('tag'));
    }
}
