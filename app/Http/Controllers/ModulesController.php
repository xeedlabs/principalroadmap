<?php

namespace App\Http\Controllers;

use App\Models\Module;

class ModulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Module::with('roadmaps')->get();

        return \View::make('modules.index', compact('modules'));
    }

}
