<?php

namespace App\Http\Controllers\Admin;

use App\Models\Tag;
use App\Models\User;
use App\Models\Author;
use App\Models\Lesson;
use App\Models\Module;
use App\Models\Resource;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LessonModuleStoreRequest;
use App\Http\Requests\Admin\LessonModuleUpdateRequest;

class LessonModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Module $module
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Module $module)
    {
        $lessons = $module->lessons()->with([
            'author.user',
            'tags',
            'resources',
        ])->orderBy('bonus')->orderBy('order')->orderBy('created_at', 'DESC')->get();

        return \View::make('admin.modules.lessons.index', compact('module', 'lessons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Module $module
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Module $module)
    {
        $lesson = new Lesson();

        $authors = Author::get()->lists('name', 'uuid');
        $selected_author = null;

        $tags = Tag::get()->lists('name', 'slug');
        $selected_tags = [];

        $resources          = Resource::get();
        $select_resources = [];
        foreach ($resources as $resource) {
            $select_resources[$resource->slug] = "({$resource->type})\t{$resource->title}";
        }
        $resources = $select_resources;
        $selected_resources = [];

        $types = Lesson::types();
        $selected_type = null;

        return \View::make('admin.modules.lessons.create',
            compact('module', 'lesson', 'authors', 'selected_author', 'tags', 'selected_tags', 'resources',
                'selected_resources', 'types', 'selected_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param LessonModuleStoreRequest $request
     * @param Module                   $module
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LessonModuleStoreRequest $request, Module $module)
    {
        $lesson = new Lesson($request->all());

        $user = \Auth::user();
        if ($user->isAdmin()) {
            $author = User::whereUuid($request->author)->first()->author;
        } else {
            $author = $user->author;
        }

        $lesson->author()->associate($author);
        $lesson->module()->associate($module);
        $lesson->save();
        $lesson->tags()->sync(Tag::whereIn('slug', $request->get('tags'))->get());
        $lesson->resources()->sync(Resource::whereIn('slug', $request->get('resources'))->get());

        return \Redirect::route('admin.modules.lessons.index', $module);
    }

    /**
     * Display the specified resource.
     *
     * @param Module $module
     * @param Lesson $lesson
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(Module $module, Lesson $lesson)
    {
        return \View::make('admin.modules.lessons.show', compact('module', 'lesson'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Module $module
     * @param Lesson $lesson
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module, Lesson $lesson)
    {
        $authors = Author::get()->lists('name', 'uuid');
        $selected_author = $lesson->author->uuid;

        $tags = Tag::get()->lists('name', 'slug');
        $selected_tags = $lesson->tags->pluck('slug')->toArray();

        $resources          = Resource::get();
        $select_resources = [];
        foreach ($resources as $resource) {
            $select_resources[$resource->slug] = "({$resource->type})\t{$resource->title}";
        }
        $resources = $select_resources;
        $selected_resources = $lesson->resources->pluck('slug')->toArray();

        $types = Lesson::types();
        $selected_type = null;

        return \View::make('admin.modules.lessons.edit',
            compact('module', 'lesson', 'authors', 'selected_author', 'tags', 'selected_tags', 'resources',
                'selected_resources', 'types', 'selected_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param LessonModuleUpdateRequest $request
     * @param Module                    $module
     * @param Lesson                    $lesson
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(LessonModuleUpdateRequest $request, Module $module, Lesson $lesson)
    {
        $lesson->fill($request->all());

        if (\Auth::user()->isAdmin()) {
            $author = User::whereUuid($request->author)->first()->author;
            $lesson->author()->associate($author);
        }

        $lesson->tags()->sync(Tag::whereIn('slug', $request->get('tags'))->get());
        $lesson->resources()->sync(Resource::whereIn('slug', $request->get('resources'))->get());
        $lesson->save();

        return \Redirect::route('admin.modules.lessons.index', $module);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Module $module
     * @param Lesson $lesson
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Module $module, Lesson $lesson)
    {
        $lesson->delete();

        return \Redirect::route('admin.modules.lessons.index', $module);
    }
}
