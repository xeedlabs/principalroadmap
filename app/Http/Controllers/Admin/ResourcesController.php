<?php

namespace App\Http\Controllers\Admin;

use App\Models\Resource;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ResourceStoreRequest;
use App\Http\Requests\Admin\ResourceUpdateRequest;
use App\Http\Requests\Admin\ResourceUploadRequest;

class ResourcesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resources = Resource::orderBy('created_at', 'DESC')->get();

        return \View::make('admin.resources.index', compact('resources'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $resource = new Resource();

        return \View::make('admin.resources.create', compact('resource'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ResourceStoreRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ResourceStoreRequest $request)
    {
        $resource = new Resource($request->all());
        $resource->save();

        return \Redirect::route('admin.resources.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Resource $resource
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(Resource $resource)
    {
        return \View::make('admin.resources.show', compact('resource'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Resource $resource
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Resource $resource)
    {
        return \View::make('admin.resources.edit', compact('resource'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ResourceUpdateRequest $request
     * @param Resource              $resource
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ResourceUpdateRequest $request, Resource $resource)
    {
        $resource->fill($request->all());
        $resource->save();

        return \Redirect::route('admin.resources.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Resource $resource
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Resource $resource)
    {
        $resource->delete();

        return \Redirect::route('admin.resources.index');
    }

    public function upload(ResourceUploadRequest $request)
    {
        $file = $request->file('resource-file');

        $relative_uri = 'files/resources';

        $resource = new Resource();
        $resource->title = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension());
        $resource->type = $file->getMimeType();
        $resource->save();

        $resource->url = \URL::to($relative_uri . '/' . $resource->slug . '.' . $file->getClientOriginalExtension());
        $resource->url = \Config::get('app.url') . '/' . $relative_uri . '/' . $resource->slug . '.' . $file->getClientOriginalExtension();
        $destination_directory = public_path() . '/' . $relative_uri;
        $file->move($destination_directory, $resource->slug . '.' . $file->getClientOriginalExtension());
        $resource->save();

        return \Response::json($resource);
    }
}
