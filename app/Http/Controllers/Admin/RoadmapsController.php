<?php

namespace App\Http\Controllers\Admin;

use App\Models\Group;
use App\Models\Module;
use App\Models\Roadmap;
use App\Models\Principal;
use App\Models\ModuleRoadmap;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RoadmapStoreRequest;
use App\Http\Requests\Admin\RoadmapUpdateRequest;

class RoadmapsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roadmaps = Roadmap::with(['admin.user', 'principal.user', 'modules'])->orderBy('created_at',
            'DESC')->paginate();

        return \View::make('admin.roadmaps.index', compact('roadmaps'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roadmap = new Roadmap();

        $groups          = Group::get()->lists('name', 'id');
        $selected_groups = [];

        $principals = Principal::with('user')->get()->lists('name', 'uuid');

        $modules          = Module::get()->lists('title', 'slug');
        $selected_modules = [];

        return \View::make('admin.roadmaps.create',
            compact('roadmap', 'groups', 'selected_groups', 'principals', 'modules', 'selected_modules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RoadmapStoreRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RoadmapStoreRequest $request)
    {
        $roadmap = new Roadmap($request->all());
        $roadmap->admin()->associate(\Auth::user()->admin);
        $roadmap->save();
        $roadmap->groups()->sync(Group::whereIn('id', $request->get('groups'))->get());

        $module_slugs = $request->get('modules');
        for ($i = count($request->get('modules')); $i > 0; $i--) {
            $module = Module::whereSlug($module_slugs[$i-1])->first();

            $module_roadmap = new ModuleRoadmap();
            $module_roadmap->module_id = $module->id;
            $module_roadmap->roadmap_id = $roadmap->id;
            $module_roadmap->order = 1;
            $module_roadmap->save();
        }

        return \Redirect::route('admin.roadmaps.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Roadmap $roadmap
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(Roadmap $roadmap)
    {
        return \Redirect::route('admin.roadmaps.modules.index', ['roadmaps' => $roadmap]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Roadmap $roadmap
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Roadmap $roadmap)
    {
        $groups          = Group::get()->lists('name', 'id');
        $selected_groups = $roadmap->groups->lists('id')->toArray();

        $principals = Principal::with('user')->get()->lists('name', 'uuid');

        $modules = Module::get()->lists('title', 'slug');

        $selected_modules = $roadmap->modules->lists('slug')->toArray();

        return \View::make('admin.roadmaps.edit', compact('roadmap', 'groups', 'selected_groups', 'principals', 'modules', 'selected_modules'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RoadmapUpdateRequest $request
     * @param Roadmap              $roadmap
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RoadmapUpdateRequest $request, Roadmap $roadmap)
    {
        $roadmap->fill($request->all());
        $roadmap->force_modules_enabled = $request->get('force_modules_enabled');
        $roadmap->save();
        $roadmap->groups()->sync(Group::whereIn('id', $request->get('groups'))->get());

        $module_slugs = $request->get('modules');
        $modules = Module::whereIn('slug', $module_slugs)->get();
        ModuleRoadmap::whereRoadmapId($roadmap->id)->whereNotIn('module_id', $modules->pluck('id', 'id'))->delete();
        $last_order = 0;
        if (null !== ($last_module = ModuleRoadmap::whereRoadmapId($roadmap->id)->orderBy('order', 'desc')->first())) {
            $last_order = $last_module->order;
        }

        for ($i = count($request->get('modules')); $i > 0; $i--) {
            $module = Module::whereSlug($module_slugs[$i-1])->first();

            if (null === ($module_roadmap = ModuleRoadmap::whereModuleId($module->id)->whereRoadmapId($roadmap->id)->first())) {
                $module_roadmap = new ModuleRoadmap();
                $module_roadmap->module_id = $module->id;
                $module_roadmap->roadmap_id = $roadmap->id;
                $module_roadmap->order = ++$last_order;
                $module_roadmap->save();
            }
        }

        return \Redirect::route('admin.roadmaps.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Roadmap $roadmap
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Roadmap $roadmap)
    {
        $roadmap->delete();

        return \Redirect::route('admin.roadmaps.index');
    }
}
