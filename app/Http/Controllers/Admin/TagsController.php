<?php

namespace App\Http\Controllers\Admin;

use App\Models\Tag;
use App\Services\URLParser;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TagStoreRequest;
use App\Http\Requests\Admin\TagUpdateRequest;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::with('lessons')->orderBy('created_at', 'DESC')->get();

        return \View::make('admin.tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tag = new Tag();

        return \View::make('admin.tags.create', compact('tag'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TagStoreRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TagStoreRequest $request)
    {
        $tag = new Tag($request->all());
        $tag->save();

        return \Redirect::route('admin.tags.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Tag $tag
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(Tag $tag)
    {
        return \View::make('admin.tags.show', compact('tag'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Tag $tag
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        return \View::make('admin.tags.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TagUpdateRequest $request
     * @param Tag              $tag
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(TagUpdateRequest $request, Tag $tag)
    {
        $tag->fill($request->all());
        $tag->save();

        return \Redirect::route('admin.tags.index', $tag);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Tag $tag
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Tag $tag)
    {
        $tag->delete();

        return \Redirect::route('admin.tags.index',
            ['page' => URLParser::getParameter(\Request::header('referer'), 'page')]);
    }
}
