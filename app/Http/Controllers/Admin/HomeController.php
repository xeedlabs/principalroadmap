<?php

namespace App\Http\Controllers\Admin;

use App\Models\Principal;
use App\Models\Roadmap;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {

        $roadmaps = Roadmap::with([
            'admin.user',
            'principal.user',
            'modules',
            'modules.lessons',
            'modules.requiredLessons',
            'modules.requiredLessons.principals',
        ])->orderBy('created_at', 'DESC')->paginate();

        $principals = Principal::with('user')->get();

        return \View::make('admin.home.index', compact('roadmaps', 'principals'));
    }
}
