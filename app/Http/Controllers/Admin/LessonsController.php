<?php

namespace App\Http\Controllers\Admin;

use App\Models\Tag;
use App\Models\User;
use App\Models\Author;
use App\Models\Lesson;
use App\Models\Module;
use App\Models\Resource;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LessonStoreRequest;
use App\Http\Requests\Admin\LessonUpdateRequest;

class LessonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        /*$lessons = Lesson::with([
            'author.user',
            'tags',
            'resources',
        ])->orderBy('bonus')->orderBy('order')->orderBy('created_at', 'DESC')->paginate();*/
        $lessons = Lesson::with([
            'author.user',
            'tags',
            'resources',
        ])->orderBy('bonus')->orderBy('order')->orderBy('created_at', 'DESC')->get();
        return \View::make('admin.lessons.index', compact('module', 'lessons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lesson = new Lesson();

        $authors = Author::get()->lists('name', 'uuid');
        $selected_author = null;

        $modules = Module::get()->lists('title', 'slug');
        $selected_module = null;

        $tags = Tag::get()->lists('name', 'slug');
        $selected_tags = [];

        $resources          = Resource::get();
        $select_resources = [];
        foreach ($resources as $resource) {
            $select_resources[$resource->slug] = "({$resource->type})\t{$resource->title}";
        }
        $resources = $select_resources;
        $selected_resources = [];

        $types = Lesson::types();
        $selected_type = null;

        return \View::make('admin.lessons.create',
            compact('lesson', 'authors', 'selected_author', 'modules', 'selected_module', 'tags', 'selected_tags', 'resources',
                'selected_resources', 'types', 'selected_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param LessonStoreRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LessonStoreRequest $request)
    {
        $lesson = new Lesson($request->all());

        $user = \Auth::user();
        if ($user->isAdmin()) {
            $author = User::whereUuid($request->author)->first()->author;
        } else {
            $author = $user->author;
        }

        $module = Module::whereSlug($request->get('module'))->first();

        $lesson->author()->associate($author);
        $lesson->module()->associate($module);
        $lesson->save();
        $lesson->tags()->sync(Tag::whereIn('slug', $request->get('tags'))->get());
        $lesson->resources()->sync(Resource::whereIn('slug', $request->get('resources'))->get());

        return \Redirect::route('admin.lessons.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Lesson $lesson
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(Lesson $lesson)
    {
        return \View::make('admin.lessons.show', compact('lesson'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Lesson $lesson
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Lesson $lesson)
    {
        $authors = Author::get()->lists('name', 'uuid');
        $selected_author = $lesson->author->uuid;

        $modules = Module::get()->lists('title', 'slug');
        $selected_module = $lesson->module->slug;

        $tags = Tag::get()->lists('name', 'slug');
        $selected_tags = $lesson->tags->pluck('slug')->toArray();

        $resources          = Resource::get();
        $select_resources = [];
        foreach ($resources as $resource) {
            $select_resources[$resource->slug] = "({$resource->type})\t{$resource->title}";
        }
        $resources = $select_resources;
        $selected_resources = $lesson->resources->pluck('slug')->toArray();

        $types = Lesson::types();
        $selected_type = null;

        return \View::make('admin.lessons.edit',
            compact('lesson', 'authors', 'selected_author', 'modules', 'selected_module', 'tags', 'selected_tags', 'resources',
                'selected_resources', 'types', 'selected_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param LessonUpdateRequest $request
     * @param Lesson                    $lesson
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(LessonUpdateRequest $request, Lesson $lesson)
    {
        $lesson->fill($request->all());

        if (\Auth::user()->isAdmin()) {
            $author = User::whereUuid($request->author)->first()->author;
            $lesson->author()->associate($author);
        }

        $module = Module::whereSlug($request->get('module'))->first();

        $lesson->tags()->sync(Tag::whereIn('slug', $request->get('tags'))->get());
        $lesson->module()->associate($module);
        $lesson->resources()->sync(Resource::whereIn('slug', $request->get('resources'))->get());
        $lesson->save();

        return \Redirect::route('admin.lessons.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Lesson $lesson
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Lesson $lesson)
    {
        $lesson->delete();

        return \Redirect::route('admin.lessons.index');
    }
}
