<?php

namespace App\Http\Controllers\Admin;

use App\Models\Module;
use App\Models\Roadmap;
use App\Models\ModuleRoadmap;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ModuleRoadmapUpdateRequest;

class ModuleRoadmapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Roadmap $roadmap
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Roadmap $roadmap)
    {
        $modules = $roadmap->modules()->with([
            'authors.user',
            'author.user'
        ])->orderBy('pivot_bonus')->orderBy('pivot_order')->orderBy('pivot_created_at', 'DESC')->paginate();

        return \View::make('admin.roadmaps.modules.index', compact('roadmap', 'modules'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Roadmap $roadmap
     * @param Module  $module
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Roadmap $roadmap, Module $module)
    {
        $module_roadmap = ModuleRoadmap::getByModuleRoadmap($module, $roadmap);

        return \View::make('admin.roadmaps.modules.edit', compact('module_roadmap'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ModuleRoadmapUpdateRequest $request
     * @param Roadmap                    $roadmap
     * @param Module                     $module
     *
     * @return \Illuminate\Http\Response
     */
    public function update(ModuleRoadmapUpdateRequest $request, Roadmap $roadmap, Module $module)
    {
        $module_roadmap = ModuleRoadmap::getByModuleRoadmap($module, $roadmap);
        $module_roadmap->fill($request->all());

        $module_roadmap->save();

        return \Redirect::route('admin.roadmaps.modules.index', $roadmap);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Roadmap $roadmap
     * @param Module  $module
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Roadmap $roadmap, Module $module)
    {
        $module_roadmap = ModuleRoadmap::getByModuleRoadmap($module, $roadmap);
        $module_roadmap->delete();

        return \Redirect::route('admin.roadmaps.modules.index', $roadmap);
    }
}
