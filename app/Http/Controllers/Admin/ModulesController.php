<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Author;
use App\Models\Module;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ModuleStoreRequest;
use App\Http\Requests\Admin\ModuleUpdateRequest;

class ModulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        //$modules = Module::with(['authors.user', 'author.user', 'lessons'])->orderBy('created_at', 'desc')->paginate();
        $modules = Module::with(['authors.user', 'author.user', 'lessons'])->orderBy('created_at', 'desc')->get();
        
        return \View::make('admin.modules.index', compact('roadmap', 'modules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $module = new Module();

        $authors          = Author::get()->lists('name', 'uuid');
        $selected_author  = null;
        $selected_authors = [];
        $leaders          = Module::getAvailableLeaders()->lists('name', 'joomla_user_id');
        $selected_leader  = null;

        return \View::make('admin.modules.create',
            compact('module', 'authors', 'selected_author', 'selected_authors', 'leaders', 'selected_leader'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ModuleStoreRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ModuleStoreRequest $request)
    {
        $module = new Module($request->all());

        $user = \Auth::user();
        if ($user->isAdmin()) {
            $author = User::whereUuid($request->author)->first()->author;
        } else {
            $author = $user->author;
        }

        $module->author()->associate($author);
        $module->save();
        $module->authors()->sync(User::whereIn('uuid', $request->get('authors'))->get());

        return \Redirect::route('admin.modules.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Module $module
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(Module $module)
    {
        
        return \Redirect::route('admin.modules.lessons.index', compact('module'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Module $module
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module)
    {
        $authors          = Author::get();
        $authors          = array_combine($authors->pluck('uuid')->toArray(), $authors->pluck('name')->toArray());
        $selected_author  = $module->author->uuid;
        $selected_authors = $module->authors->pluck('uuid')->toArray();

        // Todo: Get leaders correctly
        $leaders         = Module::getAvailableLeaders()->lists('name', 'joomla_user_id');
        $selected_leader = null;

        return \View::make('admin.modules.edit',
            compact('module', 'authors', 'selected_author', 'selected_authors', 'leaders', 'selected_leader'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ModuleUpdateRequest $request
     * @param Module              $module
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ModuleUpdateRequest $request, Module $module)
    {
        $module->fill($request->all());

        if (\Auth::user()->isAdmin()) {
            $author = User::whereUuid($request->author)->first()->author;
            $module->author()->associate($author);
        }

        $module->save();
        $module->authors()->sync(User::whereIn('uuid', $request->get('authors'))->get());

        return \Redirect::route('admin.modules.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Module $module
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Module $module)
    {
        $module->delete();

        return \Redirect::route('admin.modules.index');
    }
}
