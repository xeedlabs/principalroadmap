<?php

namespace App\Http\Controllers;

use App\Models\Resource;

class ResourcesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resources = Resource::get();

        return \View::make('resources.index', compact('resources'));
    }

    /**
     * Display the specified resource.
     *
     * @param  Tag $tag
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        $tag = Tag::with('lessons')->find($tag->id);

        return \View::make('tags.show', compact('tag'));
    }
}