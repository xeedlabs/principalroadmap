<?php

namespace App\Http\Controllers;

use App\Models\Roadmap;
use App\Models\Module;

class RoadmapsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roadmaps = Roadmap::get();

        return \View::make('roadmaps.index', compact('roadmaps'));
    }

    /**
     * Display the specified resource.
     *
     * @param Roadmap $roadmap
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function show(Roadmap $roadmap)
    {
        $roadmap = Roadmap::with([
            'modules',
            'modules.requiredLessons',
            'modules.requiredLessons.principals',
            'requiredModules',
            'requiredModules.bonusLessons',
            'requiredModules.bonusLessons.principals',
            'requiredModules.requiredLessons',
            'requiredModules.requiredLessons.principals',
        ])->whereId($roadmap->getKey())->first();

        $principal = null;
        $completed_lessons = collect([]);
        $percentage_completed = 0;
        if (($user = \Auth::user()) && $user->isPrincipal()) {
            $completed_lessons = $user->principal->completedLessons;
            $percentage_completed = $roadmap->percentageCompleted();
            $principal = $user->principal;
        }
        //get user images
        $images = ($user)?$user->getAvatarImages():false;
        $leaders = Module::getLeadersData();

        return \View::make('roadmaps.show', compact('roadmap', 'principal', 'completed_lessons', 'percentage_completed', 'user', 'images', 'leaders'));
    }
}
