<?php

namespace App;

class AjaxResponse
{
    protected $success = false;
    protected $redirect = '';
    protected $errors = [];

    public function __set($name, $value) {
        if (property_exists(self::class, $name)) {
            $set = 'set' . ucfirst($name);
            return $this->$set($value);
        } else {
            $this->$name = $value;
        }

        return null;
    }

    public function __get($name) {
        if (property_exists(self::class, $this->$name)) {
            $get = 'get' . ucfirst($name);
            return $this->$get();
        }

        return null;
    }

    public function setSuccess($value)
    {
        return $this->success = !! $value;
    }

    public function getSuccess()
    {
        return $this->success;
    }

    public function setRedirect($value)
    {
        if (is_string($value)) {
            return $this->redirect = $value;
        }

        return null;
    }

    public function getRedirect()
    {
        return $this->redirect;
    }

    public function addError($value)
    {
        if (is_string($value)) {
            $this->errors[] = $value;
        }
    }

    public function setErrors($value)
    {
        if (is_string($value)) {
            $value =[$value];
        } elseif (!is_array($value)) {
            $value = null;
        }

        if (null === $value) {
            return $value;
        }

        return $this->errors = $value;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }

    public function __toString()
    {
        return $this->toJson();
    }
}
