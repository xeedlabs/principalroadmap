<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Collection;

Trait ManyToManyRelationTrait
{
    public function models($class)
    {
        return $this->belongsToMany($class)->withTimestamps();
    }

    public function addModel($class, $foreign_key, $field, $value)
    {
        $added_model = null;

        $where_field = 'where' . $field;

        $where_model_id = 'where' . Str::studly($foreign_key);

        $model = is_string($value) ? $class::$where_field($value)->first() : $value;

        if (is_a($model, $class)) {
            if (!$this->models($class)->$where_model_id($model->id)->count()) {
                $this->models($class)->save($model);
            }

            $added_model = $model;
        }

        return $added_model;
    }

    public function addModels($class, $foreign_key, $field, $values)
    {
        if (is_string($values) || is_a($values, $class)) {
            $values = [$values];
        }

        $collection = new Collection();

        if (is_array($values) || is_a($values, Collection::class)) {
            foreach ($values as $value) {
                if (null !== ($model = $this->addModel($class, $foreign_key, $field, $value))) {
                    $collection->add($model);
                }
            }
        }

        return $collection;
    }

    public function removeModel($class, $field, $value)
    {
        $removed_model = null;

        $where_field = 'where' . $field;

        $model = is_string($value) ? $class::$where_field($value)->first() : $value;

        if (is_a($model, $class)) {
            if ($this->models($class)->detach($model->id)) {
                $removed_model = $model;
            }
        }

        return $removed_model;
    }

    public function removeModels($class, $field, $values)
    {
        if (is_string($values) || is_a($values, $class)) {
            $values = [$values];
        }

        $collection = new Collection();

        if (is_array($values) || is_a($values, Collection::class)) {
            foreach ($values as $value) {
                if (null !== ($model = $this->removeModel($class, $field, $value))) {
                    $collection->add($model);
                }
            }
        }

        return $collection;
    }

    public function setModels($class, $field, $values)
    {
        if (is_string($values) || is_a($values, $class)) {
            $values = [$values];
        }

        $models = [];

        if (is_array($values) || is_a($values, Collection::class)) {
            $where_field = 'where' . $field;

            foreach ($values as $value) {
                $model = is_string($value) ? $class::$where_field($value)->first() : $value;

                if (is_a($model, $class)) {
                    $models[$model->id] = $model;
                }
            }
        }

        $this->models($class)->sync(array_keys($models));
        $collection = collect($models);

        return $collection;
    }

    public function hasModel($class, $foreign_key, $field, $value)
    {
        $has = false;

        $where_field = 'where' . $field;

        $where_model_id = 'where' . Str::studly($foreign_key);

        $model = is_string($value) ? $class::$where_field($value)->first() : $value;

        if (is_a($model, $class)) {
            $has = !!$this->models($class)->$where_model_id($model->id)->count();
        }

        return $has;
    }

    public function hasModels($class, $foreign_key, $field, $values)
    {
        $has = false;

        if (is_string($values)) {
            $values = [$values];
        } elseif (is_a($values, $class)) {
            $values = new Collection($values);
        }

        if (is_array($values)) {
            foreach ($values as $value) {
                if (($has = $this->hasModel($class, $foreign_key, $field, $value))) {
                    break;
                }
            }
        } elseif (is_a($values, Collection::class)) {
            $has = !!$values->intersect($this->models($class)->getResults())->count();
        }

        return $has;
    }
}
