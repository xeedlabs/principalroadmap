<?php

namespace App\Models;

use Cache;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, RoleTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'email'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function admin()
    {
        return $this->hasOne(Admin::class, 'id');
    }

    public function author()
    {
        return $this->hasOne(Author::class, 'id');
    }

    public function principal()
    {
        return $this->hasOne(Principal::class, 'id');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class)->withTimestamps();
    }

    public function isOwner($model)
    {
        $isOwner = false;

        if (is_a($model, Module::class) || is_a($model, Lesson::class)) {
            $isOwner = $this->id == $model->author_id;
        }

        return $isOwner;
    }

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function isAdmin()
    {
        return Cache::remember('user.isAdmin', \Config::get('cache.ttl'), function () {
            $hasRecordAdmin = Cache::remember('user.hasRecordAdmin', \Config::get('cache.ttl'), function () {
                return !!$this->admin;
            });

            $hasRoleAdmin = Cache::remember('user.hasRoleAdmin', \Config::get('cache.ttl'), function () {
                return $this->hasRole(Role::ADMIN_NAME);
            });

            return !!($hasRoleAdmin && $hasRecordAdmin);
        });
    }

    public function isAuthor()
    {
        return Cache::remember('user.isAuthor', \Config::get('cache.ttl'), function () {
            $hasRecordAuthor = Cache::remember('user.hasRecordAuthor', \Config::get('cache.ttl'), function () {
                return !!$this->author;
            });

            $hasRoleAuthor = Cache::remember('user.hasRoleAuthor', \Config::get('cache.ttl'), function () {
                return $this->hasRole(Role::AUTHOR_NAME);
            });

            return !!($hasRoleAuthor && $hasRecordAuthor);
        });
    }

    public function isPrincipal()
    {
        return Cache::remember('user.isPrincipal', \Config::get('cache.ttl'), function () {
            $hasRecordPrincipal = Cache::remember('user.hasRecordPrincipal', \Config::get('cache.ttl'), function () {
                return !!$this->principal;
            });

            $hasRolePrincipal = Cache::remember('user.hasRolePrincipal', \Config::get('cache.ttl'), function () {
                return $this->hasRole(Role::PRINCIPAL_NAME);
            });

            return !!($hasRolePrincipal && $hasRecordPrincipal);
        });
    }

    public function getAvatarImages()
    {
        //$images = new Collection();
        $response = false;
        try {
            $response = json_decode(file_get_contents(env('JOOMLA_BRIDGE') . '?userInfo='.$this->id ));
        } catch (\Exception $e) {
            $response = $e;
        }

        return $response;
    }
}
