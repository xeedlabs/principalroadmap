<?php

namespace App\Models;

class Principal extends Model
{
    use UserTrait;

    protected $fillable = ['id'];

    public $incrementing = false;

    public $timestamps = false;

    public static function make($attributes = [], User $user = null)
    {
        return self::makeRelation('principal', $attributes, $user);
    }

    public function lessons()
    {
        return $this->belongsToMany(Lesson::class)->withTimestamps();
    }

    public function completedLessons()
    {
        return $this->lessons();
    }
}
