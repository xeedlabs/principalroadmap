<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{
    public static function resolveIdFromUser($user = null)
    {
        $id = \Auth::id();

        if (is_a($user, User::class) || is_a($user, Admin::class) || is_a($user, Author::class) || is_a($user, Principal::class)) {
            $id = $user->id;
        } elseif (is_numeric($user)) {
            $id = $user;
        }

        return $id;
    }
}
