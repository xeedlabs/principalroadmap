<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\JoinClause;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;

class Roadmap extends Model implements SluggableInterface
{
    use SluggableTrait, TaggableTrait;

    protected $fillable = [
        'title',
        'description',
        'image_url',
        'force_modules_enabled'
    ];

    protected $sluggable = ['build_from' => 'title'];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function principal()
    {
        return $this->belongsTo(Principal::class);
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class)->withTimestamps();
    }

    public function modules()
    {
        return $this->belongsToMany(Module::class)->withTimestamps()->withPivot(['id', 'order', 'bonus']);
    }

    public function modulesWithOrderAsc()
    {
        return $this->modules()->orderBy('module_roadmap.order', 'ASC');
    }

    public function requiredModules()
    {
        return $this->modulesWithOrderAsc()->wherePivot('bonus', '=', false);
    }

    public function bonusModules()
    {
        return $this->modulesWithOrderAsc()->wherePivot('bonus', '=', true);
    }

    public function pivotModules()
    {
        return $this->hasMany(ModuleRoadmap::class);
    }

    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        return $this->slug;
    }

    public function canTakeModule(Module $module, $principal = null)
    {
        $can = false;
        $order = null;

        foreach ($this->modules as $avaiable_module) {
            if ($avaiable_module->getKey() == $module->getKey()) {
                $order = $avaiable_module->pivot->order;
                break;
            }
        }

        if (! is_null($order)) {
            foreach ($this->modules as $module) {
                if ($module->pivot->order >= $order) {
                    continue;
                } else {
                    if (! $module->isCompletedByPrincipal($principal)) {
                        return false;
                    }
                }
            }

            $can = true;
        }

        return $can;
    }

    public function percentageCompleted($principal = null)
    {
        $id = self::resolveIdFromUser($principal);

        $prefix = \DB::getTablePrefix();

        $row = \DB::table('module_roadmap as mr')
            ->join('lessons as l', function(JoinClause $join) {
                $join->on('mr.module_id', '=', 'l.module_id')
                    ->where('l.bonus', '=', false);
            })
            ->leftJoin('lesson_principal as lp', function(JoinClause $join) use($id) {
                $join->on('l.id', '=', 'lp.lesson_id')
                    ->where('lp.principal_id', '=', $id);
            })
            ->where('mr.roadmap_id', '=', $this->getKey())
            ->select(\DB::raw("IF(COUNT({$prefix}l.id), ROUND(100 * COUNT({$prefix}lp.id) / COUNT({$prefix}l.id)), 0) as percentage_completed"))
            ->first();

        return $row->percentage_completed;
    }

    public function isCompletedByPrincipal(Principal $principal = null)
    {
        $isCompleted = true;
        foreach ($this->modules as $module) {
            if (!$module->isCompletedByPrincipal($principal)) {
                return false;
            }
        }

        return $isCompleted;
    }

    public function isStartedByPrincipal(Principal $principal = null)
    {
        $isStarted = false;
        foreach ($this->modules as $module) {
            if (!$module->isStartedByPrincipal($principal)) {
                return true;
            }
        }

        return $isStarted;
    }

    public function completedByPrincipals(Collection $principals = null)
    {
        $completed = collect([]);
        $principals =  $principals?: Principal::with('user')->get();

        foreach ($principals as $principal) {
            if ($this->isCompletedByPrincipal($principal)) {
                $completed->push($principal);
            }
        }

        return $completed;
    }

    public function startedByPrincipals(Collection $principals = null)
    {
        $started = collect([]);
        $principals =  $principals?: Principal::with('user')->get();

        foreach ($principals as $principal) {
            if ($this->isStartedByPrincipal($principal)) {
                $started->push($principal);
            }
        }

        return $started;
    }

    public function setForceModulesEnabledAttribute($value) {
        if (is_string($value) && $value == 'on') {
            $value = true;
        }

        return $this->attributes['force_modules_enabled'] = (bool) $value;
    }
}
