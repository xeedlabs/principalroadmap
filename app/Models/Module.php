<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;

class Module extends Model implements SluggableInterface
{
    use SluggableTrait, TaggableTrait;

    protected $fillable = [
        'joomla_user_id',
        'title',
        'description',
        'image_url',
        'order',
        'discussion_link',
        'bonus'
    ];

    protected $sluggable = ['build_from' => 'title'];

    public static function getAvailableLeaders()
    {
        $leaders = new Collection();

        try {
            $response = json_decode(file_get_contents(env('JOOMLA_BRIDGE') . '?leaders'));

            foreach ($response->leaders as $joomla_user_id => $name) {
                $leaders->push([
                    'joomla_user_id' => $joomla_user_id,
                    'name' => $name,
                ]);
            }
        } catch (\Exception $e) {
            // Do nothing
        }

        return $leaders;
    }
    public static function getLeadersData()
    {
        $leaders = array();

        try {
            $response = json_decode(file_get_contents(env('JOOMLA_BRIDGE') . '?leadersInfo=1'));

            foreach ($response->leaders as $joomla_user_id => $leader) {
                $leaders[$joomla_user_id] = [
                    'joomla_user_id' => $joomla_user_id,
                    'name' => $leader->name,
                    'avatar' => $leader->avatar,
                    'thumb' => $leader->thumb
                    ];
            }
        } catch (\Exception $e) {
            // Do nothing
        }

        return $leaders;
    }

    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    public function authors()
    {
        return $this->belongsToMany(Author::class)->withTimestamps();
    }

    public function roadmaps()
    {
        return $this->belongsToMany(Roadmap::class)->withTimestamps()->withPivot(['id', 'order', 'bonus']);
    }

    public function lessons()
    {
        return $this->hasMany(Lesson::class);
    }

    public function lessonsWithOrderAsc()
    {
        return $this->lessons()->orderBy('order', 'ASC');
    }

    public function requiredLessons()
    {
        return $this->lessonsWithOrderAsc()->whereBonus(false);
    }

    public function bonusLessons()
    {
        return $this->lessonsWithOrderAsc()->whereBonus(true);
    }

    public function completedRequiredLessonsByPrincipal(Principal $principal = null)
    {
        $lessons = new Collection();
        foreach ($this->requiredLessons as $lesson) {
            if ($lesson->isCompletedByPrincipal($principal)) {
                $lessons->add($lesson);
            }
        }

        return $lessons;
    }

    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        return $this->slug;
    }

    public function isCompletedByPrincipal(Principal $principal = null)
    {
        $isCompleted = true;
        foreach ($this->requiredLessons as $lesson) {
            if (!$lesson->isCompletedByPrincipal($principal)) {
                return false;
            }
        }

        return $isCompleted;
    }

    public function isStartedByPrincipal(Principal $principal = null)
    {
        $isStarted = false;
        foreach ($this->requiredLessons as $lesson) {
            if ($lesson->isCompletedByPrincipal($principal)) {
                return true;
            }
        }

        return $isStarted;
    }

    public function countCompletedLessonsByPrincipal(Principal $principal = null)
    {
        $count = 0;

        foreach ($this->lessons as $lesson) {
            if ($lesson->isCompletedByPrincipal($principal)) {
                $count++;
            }
        }

        return $count;
    }

    public function getLeaderAttribute()
    {
        $leader = new \stdClass();
        $leader->joomla_user_id = $this->joomla_user_id;
        $leader->name = '';

        try {
            $response = json_decode(file_get_contents(env('JOOMLA_BRIDGE') . '?leaders=' . $this->joomla_user_id));

            $leader = new \stdClass();
            $leader->joomla_user_id = $this->joomla_user_id;
            $leader->name = $response['leader']->name;
        } catch (\Exception $e) {
            // Do nothing
        }

        return $leader;
    }
}
