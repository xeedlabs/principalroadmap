<?php

namespace App\Models;

Trait RoleTrait
{
    use ManyToManyRelationTrait;

    public function roles()
    {
        return $this->models(Role::class)->withPivot('id');
    }

    public function addToRole($name)
    {
        return $this->addModel(Role::class, 'role_id', 'name', $name);
    }

    public function addToRoles($names)
    {
        return $this->addModels(Role::class, 'role_id', 'name', $names);
    }

    public function removeFromRole($name)
    {
        return $this->removeModel(Role::class, 'name', $name);
    }

    public function removeFromRoles($names)
    {
        return $this->removeModels(Role::class, 'name', $names);
    }

    public function setRoles($names)
    {
        return $this->setModels(Role::class, 'name', $names);
    }

    public function hasRole($name)
    {
        return $this->hasModel(Role::class, 'role_id', 'name', $name);
    }

    public function hasRoles($names)
    {
        return $this->hasModels(Role::class, 'role_id', 'name', $names);
    }
}
