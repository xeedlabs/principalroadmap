<?php

namespace App\Models;

class Group extends Model
{
    use ManyToManyRelationTrait;
    const ADMINS_AUTHORS  = 8;
    const AUTHORS         = 3;
    const PRINCIPALS_K_5  = 59;
    const PRINCIPALS_6_12 = 60;
    const PRINCIPALS_K_12 = 61;

    public static function names($only_keys = false)
    {
        $names = [
            self::ADMINS_AUTHORS  => 'Pr Admins (and authors just for now)',
            self::AUTHORS         => 'Pr Authors',
            self::PRINCIPALS_K_5  => 'Principals and Admins K-5',
            self::PRINCIPALS_6_12 => 'Principals and Admins 6-12',
            self::PRINCIPALS_K_12 => 'Splash Principals and Admins K-12',
        ];

        return $only_keys ? array_keys($names) : $names;
    }

    public static function name($id)
    {
        $name  = null;
        $names = self::names();

        if (isset($names[$id])) {
            $name = $names[$id];
        }

        return $name;
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function roadmaps()
    {
        return $this->belongsToMany(Roadmap::class)->withTimestamps();
    }
}
