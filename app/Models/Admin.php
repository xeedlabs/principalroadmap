<?php

namespace App\Models;

class Admin extends Model
{
    use UserTrait;

    protected $fillable = ['id'];

    public $incrementing = false;

    public $timestamps = false;

    public function roadmaps()
    {
        return $this->hasMany(Roadmap::class);
    }

    public static function make($attributes = [], User $user = null)
    {
        return self::makeRelation('admin', $attributes, $user);
    }
}
