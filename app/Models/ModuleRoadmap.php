<?php

namespace App\Models;

class ModuleRoadmap extends Model
{
    protected $fillable = ['order', 'bonus'];

    protected $table = 'module_roadmap';

    public static function getByModuleRoadmap($module, $roadmap)
    {
        return self::with(['module', 'roadmap'])->whereModuleId($module->id)->whereRoadmapId($roadmap->id)->first();
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function roadmap()
    {
        return $this->belongsTo(Roadmap::class);
    }
}
