<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;

class Lesson extends Model implements SluggableInterface
{
    use ManyToManyRelationTrait, SluggableTrait, TaggableTrait;

    const TYPE_VIDEO = 'video';
    const TYPE_INFO = 'info';

    protected $fillable = [
        'title',
        'description',
        'image_url',
        'order',
        'bonus',
        'content',
        'ask_a_question_link',
        'type',
        'ect'
    ];

    protected $sluggable = ['build_from' => 'title'];

    protected $reserved = ['', 'create', 'upload'];

    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function principals()
    {
        return $this->models(Principal::class)->withPivot('id');
    }

    public function addPrincipal($id)
    {
        return $this->addModel(Principal::class, 'principal_id', 'id', $id);
    }

    public function addPrincipals($ids)
    {
        return $this->addModels(Principal::class, 'principal_id', 'id', $ids);
    }

    public function removePrincipal($id)
    {
        return $this->removeModel(Principal::class, 'id', $id);
    }

    public function removePrincipals($ids)
    {
        return $this->removeModels(Principal::class, 'id', $ids);
    }

    public function setPrincipals($ids)
    {
        return $this->setModels(Principal::class, 'id', $ids);
    }

    public function hasPrincipal($id)
    {
        return $this->hasModel(Principal::class, 'principal_id', 'id', $id);
    }

    public function hasPrincipals($ids)
    {
        return $this->hasModels(Principal::class, 'principal_id', 'id', $ids);
    }

    public function resources()
    {
        return $this->models(Resource::class)->withPivot('id');
    }

    public function addResource($id)
    {
        return $this->addModel(Resource::class, 'resource_id', 'id', $id);
    }

    public function addResources($ids)
    {
        return $this->addModels(Resource::class, 'resource_id', 'id', $ids);
    }

    public function removeResource($id)
    {
        return $this->removeModel(Resource::class, 'id', $id);
    }

    public function removeResources($ids)
    {
        return $this->removeModels(Resource::class, 'id', $ids);
    }

    public function setResources($ids)
    {
        return $this->setModels(Resource::class, 'id', $ids);
    }

    public function hasResource($id)
    {
        return $this->hasModel(Resource::class, 'resource_id', 'id', $id);
    }

    public function hasResources($ids)
    {
        return $this->hasModels(Resource::class, 'resource_id', 'id', $ids);
    }

    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        return $this->slug;
    }

    public function getTagsNamesText()
    {
        return $this->tags->implode('name', ',');
    }

    public function isCompletedByPrincipal(Principal $principal = null)
    {
        $id = self::resolveIdFromUser($principal);

        return $this->principals->lists('id', 'id')->has($id);
    }

    public function canBeCompletedByPrincipal(Principal $principal = null)
    {
        return !! $this->modules->roadmaps->intersect($principal->roadmaps)->count();
    }

    public static function types()
    {
        return [
            self::TYPE_VIDEO => self::TYPE_VIDEO,
            self::TYPE_INFO => self::TYPE_INFO,
        ];
    }
}
