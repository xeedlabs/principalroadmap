<?php

namespace App\Models;

Trait UserTrait
{
    public static function stub()
    {
        $self                    = new self();
        $self->relations['user'] = new User();

        return $self;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id');
    }

    public function roadmaps()
    {
        return $this->user->roadmaps();
    }

    public function getUuidAttribute()
    {
        return $this->user->uuid;
    }

    public function getEmailAttribute()
    {
        return $this->user->email;
    }

    public function getFirstNameAttribute()
    {
        return $this->user->first_name;
    }

    public function getLastNameAttribute()
    {
        return $this->user->last_name;
    }

    public function getPasswordAttribute()
    {
        return $this->user->password;
    }

    public function getRememberTokenAttribute()
    {
        return $this->user->remember_token;
    }

    public function getCreatedAtAttribute()
    {
        return $this->user->created_at;
    }

    public function getUpdatedAtAttribute()
    {
        return $this->user->updated_at;
    }

    public function getNameAttribute()
    {
        return $this->user->name;
    }

    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        return $this->uuid;
    }

    public static function makeRelation($relation, $attributes = [], User $user = null)
    {
        $self = null;

        $self = \DB::transaction(function () use ($relation, $attributes, $user) {
            $self = null;

            if (is_array($attributes)) {
                if (isset($attributes['email']) && !($user = User::whereEmail($attributes['email'])->first())) {
                    $user_attributes = [];
                    $temp_user       = new User();

                    foreach ($temp_user->getFillable() as $fillable) {
                        if (isset($attributes[$fillable])) {
                            $user_attributes[$fillable] = $attributes[$fillable];
                        }
                    }

                    $user = User::create($user_attributes);
                }
            } elseif ($attributes instanceof User) {
                $user       = $attributes;
                $attributes = [];
            }

            if ($user && $user instanceof User && is_array($attributes)) {
                $relation_model      = new self();
                $relation_attributes = [];

                foreach ($relation_model->getFillable() as $fillable) {
                    if (isset($attributes[$fillable])) {
                        $relation_attributes[$fillable] = $attributes[$fillable];
                    }
                }

                if (null === ($self = $user->$relation)) {
                    $self = $user->$relation()->create($relation_attributes);
                }
            }

            return $self;
        });

        return $self;
    }
}
