<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;

class Tag extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $fillable = ['name', 'description'];

    protected $sluggable = ['build_from' => 'name'];

    public function roadmaps()
    {
        return $this->morphedByMany(Roadmap::class, 'taggable')->withPivot('name');
    }

    public function modules()
    {
        return $this->morphedByMany(Module::class, 'taggable')->withPivot('name');
    }

    public function lessons()
    {
        return $this->morphedByMany(Lesson::class, 'taggable')->withPivot('name');
    }

    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        return $this->slug;
    }
}
