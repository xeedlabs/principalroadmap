<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;

class Resource extends Model implements SluggableInterface
{
    use ManyToManyRelationTrait, SluggableTrait;

    protected $fillable = ['title', 'url', 'description'];

    protected $sluggable = ['build_from' => 'title'];

    public function lessons()
    {
        return $this->models(Lesson::class)->withPivot('id');
    }

    public function addToLesson($id)
    {
        return $this->addModel(Lesson::class, 'lesson_id', 'id', $id);
    }

    public function addToLessons($ids)
    {
        return $this->addModels(Lesson::class, 'lesson_id', 'id', $ids);
    }

    public function removeFromLesson($id)
    {
        return $this->removeModel(Lesson::class, 'id', $id);
    }

    public function removeFromLessons($ids)
    {
        return $this->removeModels(Lesson::class, 'id', $ids);
    }

    public function setLessons($ids)
    {
        return $this->setModels(Lesson::class, 'id', $ids);
    }

    public function hasLesson($id)
    {
        return $this->hasModel(Lesson::class, 'lesson_id', 'id', $id);
    }

    public function hasLessons($ids)
    {
        return $this->hasModels(Lesson::class, 'lesson_id', 'id', $ids);
    }

    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        return $this->slug;
    }
}
