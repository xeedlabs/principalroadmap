<?php

namespace App\Models;

class Author extends Model
{
    use UserTrait;

    protected $fillable = ['id'];

    public $incrementing = false;

    public $timestamps = false;

    public function modules()
    {
        return $this->belongsToMany(Module::class);
    }

    public function canModifyModule(Module $module) {
        $modules = \Cache::remember('author.modules', \Config::get('cache.ttl'), function() {
            return $this->modules;
        });

        return $modules->contains('id', $module->id);
    }

    public static function make($attributes = [], User $user = null)
    {
        return self::makeRelation('author', $attributes, $user);
    }
}
