var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var paths = {
    'publicCss':       './public/css',
    'publicJs':        './public/js',
    'assets':          './resources/assets/',
    'js':              './resources/assets/js',
    'css':             './resources/assets/css',
    'jquery':          './resources/assets/bower/jquery/',
    'bootstrap':       './resources/assets/bower/bootstrap-sass/assets/',
    'fontawesome':     './resources/assets/bower/font-awesome/',
    'adminLTE':        './resources/assets/bower/AdminLTE/dist/',
    'adminLTEPlugins': './resources/assets/bower/AdminLTE/plugins/',
    'bower':           './resources/assets/bower/',
    'jQueryPjax':      './resources/assets/bower/jquery-pjax/'
};

elixir(function (mix) {
    mix.sass("app.scss", 'public/css/', {
        includePaths: [paths.bootstrap + 'stylesheets/', paths.fontawesome + 'scss/']
    })
        //mixing styles
        .styles([
            "vl.css",
            "vl_responsive.css"
        ], 'public/css/frontend.css')
        .copy(paths.css + '/fonts.css', paths.publicCss + '/fonts.css')
        .copy(paths.css + '/circle.css', paths.publicCss + '/circle.css')
        //copying js from site
        .copy(paths.js + '/**', paths.publicJs)
        //copying images from site
        .copy(paths.assets + 'images/**', 'public/images')

        .copy(paths.bootstrap + 'fonts/bootstrap/**', 'public/fonts/bootstrap/')
        .copy(paths.fontawesome + 'fonts/**', 'public/fonts')
        .scripts([
            paths.jquery + "dist/jquery.js",
            paths.adminLTEPlugins + 'jQueryUI/jquery-ui.min.js',
            paths.bootstrap + "javascripts/bootstrap.js"
        ], 'public/js/app.js', './')
        //copy adminlte files
        .copy(paths.assets + 'css/roadmap.css', 'public/css/roadmap.css')
        .copy(paths.adminLTE + 'css/AdminLTE.min.css', 'public/css/AdminLTE.min.css')
        .copy(paths.adminLTE + 'css/skins/skin-blue.min.css', 'public/css/AdminLTE.blue.min.css')
        .copy(paths.adminLTE + 'img/**', 'public/images/adminLTE')
        .copy(paths.adminLTE + 'js/app.min.js', 'public/js/AdminLTE.min.js')

        //BlueImp File upload plugin
        .copy(paths.bower + 'blueimp-file-upload/js/jquery.fileupload.js', paths.publicJs + '/jquery.fileupload.js')
        .copy(paths.bower + 'blueimp-file-upload/js/jquery.iframe-transport.js', paths.publicJs + '/jquery.iframe-transport.js')
        .copy(paths.bower + 'blueimp-file-upload/js/vendor/jquery.ui.widget.js', paths.publicJs + '/jquery.ui.widget.js')
        .copy(paths.bower + 'blueimp-file-upload/css/jquery.fileupload.css', paths.publicCss + '/jquery.fileupload.css')

        //summernote files
        .copy(paths.bower + 'summernote/dist/summernote.css', paths.publicCss + '/summernote.css')
        .copy(paths.bower + 'summernote/dist/summernote.min.js', paths.publicJs + '/summernote.min.js')
        .copy(paths.bower + 'summernote/plugin/summernote-ext-video.js', paths.publicJs + '/summernote-ext-video.js')

        //copy adminlte plugins
        //select2 plugin
        .copy(paths.adminLTEPlugins + 'select2/select2.full.min.js', 'public/js/select2.full.min.js')
        .copy(paths.adminLTEPlugins + 'select2/select2.min.css', 'public/css/select2.min.css')
        //jquery ui for spinner
        //.copy(paths.adminLTEPlugins + 'jQueryUI/jquery-ui.min.js', 'public/js/spinner.js')
        //slimScroll plugin
        .copy(paths.adminLTEPlugins + 'slimScroll/jquery.slimscroll.min.js', 'public/js/jquery.slimscroll.min.js')
        .copy(paths.jQueryPjax + 'jquery.pjax.js', 'public/js/jquery.pjax.js')

        //iCheck plugin
        .copy(paths.adminLTEPlugins + 'iCheck/icheck.min.js', paths.publicJs + '/icheck.min.js')
        .copy(paths.adminLTEPlugins + 'iCheck/flat/blue.css', paths.publicCss + '/iCheck.Flat.Blue.css')
        .copy(paths.adminLTEPlugins + 'iCheck/flat/blue.png', paths.publicCss + '/blue.png')
        .copy(paths.adminLTEPlugins + 'iCheck/flat/blue@2x.png', paths.publicCss + '/blue@2x.png')

        //datatables files
        .copy(paths.bower + 'datatables.net/js/jquery.dataTables.min.js', paths.publicJs + '/jquery.dataTables.min.js')
        .copy(paths.bower + 'datatables.net-bs/js/dataTables.bootstrap.min.js', paths.publicJs + '/dataTables.bootstrap.min.js')
        .copy(paths.bower + 'datatables.net-bs/css/dataTables.bootstrap.min.css', paths.publicCss + '/dataTables.bootstrap.min.css')

    ;
});

//console.log(elixir);

//elixir.config.registerWatcher("default", "resources/**");