if (!pr) {
    var pr = {};
}

pr.roadmaps = {
	lessonsToggle: function(diz, module_el) {
		if($(diz).hasClass("close-step")) {
			$('#'+module_el+' .info-cnt').hide();
			$(diz).removeClass("close-step").addClass("open-step");
			$(diz).find(".click-open").show();
		} else {
			$('#'+module_el+' .info-cnt').show();
			$(diz).removeClass("open-step").addClass("close-step");
			
			$(diz).find(".click-open").hide();
		}
	},
    init: function() {

    }
};
