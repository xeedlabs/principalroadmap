if (!admin) {
    var admin = {
        pr: {}
    };
} else if (!admin.pr) {
    admin.pr = {};
}

admin.pr.modules = {
    init: function () {
        $(document).on('ready pjax:success', function() {
            $("#author").select2({placeholder: "Select the author in charge", allowClear: true});
            $("#authors").select2({placeholder: "Select authors that can modify this module", allowClear: true});
            $("#leader").select2({placeholder: "Select leader", allowClear: true});
            $(document).pjax('.pjax-requester', '#pjax-container');
            $(document).off('submit', 'form[data-pjax]').on('submit', 'form[data-pjax]', function(event) {
                $.pjax.submit(event, '#pjax-container')
            });
            //start datatables
            $('#modules').DataTable({ordering:  true, pageLength: 20, lengthChange:false, "order": [[ 1, "desc" ]]});
        });
    }
};
