if (!admin) {
    var admin = {
        pr: {}
    };
} else if (!admin.pr) {
    admin.pr = {};
}

admin.pr.roadmaps = {
    init: function () {
        $(document).on('ready pjax:success', function() {
            $("#modules").select2({placeholder: "Select modules", allowClear: true});
            $("#principal").select2({placeholder: "Select a principal", allowClear: true});
            $("#groups").select2({placeholder: "Select groups which can access this roadmap", allowClear: true});
            /*
            $(document).pjax('.pjax-requester', '#pjax-container');
            $(document).off('submit', 'form[data-pjax]').on('submit', 'form[data-pjax]', function(event) {
                $.pjax.submit(event, '#pjax-container')
            })
            */
        });
    },

    modules: {
        init: function () {
            $(document).on('ready pjax:success', function() {
                $(document).pjax('.pjax-requester', '#pjax-container');
                $(document).off('submit', 'form[data-pjax]').on('submit', 'form[data-pjax]', function(event) {
                    $.pjax.submit(event, '#pjax-container')
                })
            });
        }
    }
};
