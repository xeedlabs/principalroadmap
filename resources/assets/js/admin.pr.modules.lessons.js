if (!admin) {
    var admin = {
        pr: {
            modules: {}
        }
    };
} else if (!admin.pr) {
    admin.pr = {
        modules: {}
    };
} else if (!admin.pr.modules) {
    admin.pr.modules = {}
}

admin.pr.modules.lessons = {
    init: function () {
        $(document).on('ready pjax:success', function () {
            var spinner = $( "#ect, #order" ).spinner();
            
            $("#author").select2({placeholder: "Select author in charge", allowClear: true});
            $("#tags").select2({placeholder: "Select tags", allowClear: true});
            $("#resources").select2({placeholder: "Select Resources", allowClear: true});

            $(document).pjax('.pjax-requester', '#pjax-container');
            $(document).off('submit', 'form[data-pjax]').on('submit', 'form[data-pjax]', function (event) {
                $.pjax.submit(event, '#pjax-container')
            });

            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass:    'iradio_flat-blue'
            });
            var url = admin.pr.base_url + 'resources/upload';

            $('#fileupload').fileupload({
                url:         url,
                dataType:    'json',
                done:        function (e, data) {
                    var resource = data.result;
                    var $resources = $("#resources");
                    $resources.select2({
                        data: [{
                            id:   resource.slug,
                            text: '(' + resource.type + ')\t' + resource.title
                        }],
                        placeholder: "Select Resources"
                    });

                    var selected_resources = $resources.val() || [];
                    selected_resources.push(data.result.slug);
                    $resources.select2('val', selected_resources);

                    $('<p/>').text(data.result.title).appendTo('#files');
                    $('#progress').addClass('hidden');
                },
                progressall: function (e, data) {
                    $('#progress').removeClass('hidden');
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                    );
                }
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
            if($("#content").length > 0) {
                $("#content").summernote({
                    height: 300,
                    toolbar: [
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                      ['font', ['strikethrough', 'superscript', 'subscript']],
                      ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['table', ['table']],
                        ['insert', ['video', 'picture', 'link']],
                        ['view', ['fullscreen', 'codeview']]
                      ]
                });
                $('.dropdown-toggle').dropdown();
            }
            //start datatables
            $('#lessons').DataTable({ordering:  true, pageLength: 20, lengthChange:false});
        });
    }
};
