if (!pr) {
    var pr = {
        roadmaps: {
            slug: ''
        }
    };
} else if (!pr.roadmaps) {
    pr.roadmaps = {
        slug: ''
    };
}

pr.roadmaps.lessons = {
    slug: '',

    init: function () {
        $(document).ready(function () {
            $('#complete').bind('click', pr.roadmaps.lessons.complete);

            // TODO: Change to correct one once modal is complete and form
            $('#shareModalBtn').bind('click', pr.roadmaps.lessons.share);
        });
    },

    complete: function () {
        var $complete = $('#complete');

        $complete.attr('disabled', true);

        $.ajax({
            dataType: 'json',
            type:     'POST',
            url:      pr.base_url + 'roadmaps/' + pr.roadmaps.slug + '/lessons/' + pr.roadmaps.lessons.slug + '/complete',
            data:     {}
        }).done(function (data) {
            $complete.attr('disabled', data.success);

            if (data.success) {
                console.log(data);
                // Todo: Change completed button to mark it
                $complete.html('COMPLETED');
                $complete.css({'borderColor': 'green', 'color': 'green'});
                //redirect to roadmap
                window.location = pr.base_url + 'roadmaps/' + pr.roadmaps.slug;
            }
        }).fail(function (x, status, error) {
            console.log(x);
        }).always(function (data) {
        });
    },

    share: function () {
        var $diz = $('#shareModalBtn');
        $diz.prop('disabled', true);
        $.ajax({
            dataType: 'json',
            type:     'POST',
            url:      pr.base_url + 'roadmaps/' + pr.roadmaps.slug + '/lessons/' + pr.roadmaps.lessons.slug + '/share',
            data:     {
                name:    $('#name').val(),
                email:   $('#email').val(),
                message: $('#message').val()
            }
        }).done(function (data) {
            if (!data.success) {
                $("#shareMessage").val(data.errors)
            } else {
                $("#shareModal").modal("hide");
            }
        }).fail(function (x, status, error) {
            // Request returns x.responseText as an array of fields each containing an array of errors
            console.log(x.responseText);
        }).always(function (data) {
            $diz.prop('disabled', false);
        });
        return false;
    }
};
