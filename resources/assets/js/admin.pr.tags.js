if (!admin) {
    var admin = {
        pr: {}
    };
} else if (!admin.pr) {
    admin.pr = {};
}

admin.pr.tags = {
    init: function () {
        $(document).on('ready pjax:success', function() {
            $(document).pjax('.pjax-requester', '#pjax-container');
            $(document).off('submit', 'form[data-pjax]').on('submit', 'form[data-pjax]', function(event) {
                $.pjax.submit(event, '#pjax-container')
            })
            //start datatables
            $('#tags').DataTable({ordering:  true, pageLength: 20, lengthChange:false});
        });
    }
};
