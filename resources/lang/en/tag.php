<?php

return [
    'fields' => [
        'placeholders' => [
            'name'       => 'Name',
            'description' => 'Description',
        ],
        'name'       => 'Name',
        'description' => 'Description',
    ]
];
