<?php

return [
    'fields' => [
        'placeholders' => [
            'author'      => 'Select the author in charge',
            'authors'     => 'Select authors that can modify this module',
            'leader'     => 'Select leader',
            'title'       => 'Title',
            'description' => 'Description',
            'image_url'   => 'http://some-awesome-image.com/great.jpg',
            'discussion_link'   => 'http://some-forum.com/id/',
        ],
        'author'       => 'Author',
        'authors'      => 'Authors',
        'leader'       => 'Leader',
        'title'        => 'Title',
        'description'  => 'Description',
        'image_url'    => 'Image link',
        'discussion_link'=> 'Discussion Url',
    ]
];
