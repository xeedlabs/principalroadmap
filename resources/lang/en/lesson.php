<?php

return [
    'fields' => [
        'placeholders'        => [
            'author'              => 'Select the author in charge',
            'module'              => 'Select the module where this lesson belongs',
            'image_url'           => 'http://some-awesome-image.com/great.jpg',
            'title'               => 'Title',
            'description'         => 'Description',
            'order'               => 'Select the order in which the principal should take this module',
            'content'             => 'Content HTML',
            'ask_a_question_link' => 'Url to Ask a question page',
            'tags'                => 'Select tags...',
            'resources'           => 'Select resources...'
        ],
        'author'              => 'Author',
        'title'               => 'Title',
        'module'              => 'Module',
        'image_url'           => 'Image link',
        'description'         => 'Description',
        'bonus'               => 'Bonus',
        'order'               => 'Order',
        'content'             => 'Content',
        'ask_a_question_link' => 'Ask a question',
        'tags'                => 'Tags',
        'resources'           => 'Resources',
        'ect'                 => 'Estimated completion time',
    ]
];
