<?php

return [
    'fields' => [
        'placeholders' => [
            'title'       => 'Title',
            'url'         => 'http://some-awesome-resource.com/great.pdf',
            'description' => 'Description',
        ],
        'title'        => 'Title',
        'url'          => 'URL',
        'description'  => 'Description',
    ]
];
