<?php

return [
    'module' => [
        'fields' => [
            'placeholders' => [
                'order'       => 'Select the order in which the principal should take this module',
            ],
            'bonus'        => 'Bonus',
            'order'        => 'Order',
        ]
    ],

    'fields' => [
        'placeholders' => [
            'groups'       => 'Select groups which can access this roadmap',
            'principal'   => 'Select a principal',
            'title'       => 'Title',
            'description' => 'Description',
            'image_url'   => 'http://some-awesome-image.com/great.jpg',
            'modules'     => 'Select modules'
        ],
        'groups'        => 'Groups',
        'principal'    => 'Principal',
        'title'        => 'Title',
        'description'  => 'Description',
        'image_url'    => 'Image link',
        'modules'      => 'Modules'
    ]
];
