<?php

return [
    'errors' => [
        'forbidden' => 'You have not enough permissions to see this.',
    ]
];
