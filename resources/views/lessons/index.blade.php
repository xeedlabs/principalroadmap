@extends('layout')

@section('body')
    <div class="body-cnt">
        <div class="container">
            <h1>Lessons</h1>
            <div class="row">
                <div class="col-sm-8">

                    @foreach($lessons as $lesson)
                    <div class="main-info-cont">
                        <p class="main-title">{{ $lesson->title }}</p>
                        <p class="text">{{ $lesson->description }}</p>

                        @if($lesson->module->roadmaps->count())
                            <br/>
                            <p>Found in the following roadmaps: </p>
                            <p>
                                @foreach($lesson->module->roadmaps as $roadmap)
                                    <a href="{{ URL::route('roadmaps.lessons.show', [$roadmap, $lesson]) }}" class="sb-text">{{ $roadmap->title }}</a>
                                @endforeach
                            </p>
                        @endif
                        <div class="div-line"></div>
                    </div>
                    @endforeach
                </div>
                <div class="col-sm-4 main-right-cnt"></div>
            </div>
        </div>
    </div>

@endsection
