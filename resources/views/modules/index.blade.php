@extends('layout')

@section('body')
    <div class="body-cnt">
        <div class="container">
            <h1>Modules</h1>
            <div class="row">
                <div class="col-sm-8">

                    @foreach($modules as $module)
                    <div class="main-info-cont">
                        <p class="main-title">{{ $module->title }}</p>
                        <p class="text">{{ $module->description }}</p>
                        @if($module->roadmaps->count())
                            <br/>
                            <p>Found in the following roadmaps: </p>
                            <p>
                                @foreach($module->roadmaps as $roadmap)
                                    <a href="{{ URL::route('roadmaps.show', $roadmap) }}" class="sb-text">{{ $roadmap->title }}</a>
                                @endforeach
                            </p>
                        @endif

                        <div class="div-line"></div>
                    </div>
                    @endforeach
                </div>
                <div class="col-sm-4 main-right-cnt"></div>
            </div>
        </div>
    </div>

@endsection

