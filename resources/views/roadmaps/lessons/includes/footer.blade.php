<script src="{{ asset('/js/pr.roadmaps.lessons.js') }}"></script>
<script>
    pr.roadmaps.slug = '{{ $roadmap->slug }}';
    pr.roadmaps.lessons.slug = '{{ $lesson->slug }}';
    pr.roadmaps.lessons.init();
</script>
