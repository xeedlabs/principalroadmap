@extends('roadmaps.lessons.layout')

@section('body')
    <div class="body-cnt">
        <div class="container">
            <h1>{{ $lesson->title }}</h1>

            <div class="row">
                <div class="col-sm-8">

{!! $lesson->content !!}
<br><br>
                    @can('roadmaps.lessons.complete')
                        @if($lesson->isCompletedByPrincipal())
                            <button type="button" class="btn btn-white bgreen" id="completed" disabled>COMPLETED</button>
                        @else
                            <button type="button" class="btn btn-white" id="complete"><span class="icon-img ii-check"></span>COMPLETE THIS STEP</button>
                        @endif
                    @endcan

                </div>
                <div class="col-sm-4 main-right-cnt">
                    <div class="white-panel">
                        <p class="wp-text"><span class="icon-img ii-time"></span> ESTIMATED TIME: {{ date('i:s', $lesson->ect * 60) }} MIN.</p>
                        <p class="wp-text sm-margin"><span class="icon-img ii-tag"></span> TAGS</p>
                        @foreach($lesson->tags as $tag)
                            <a class="tag-cnt" href="{{ URL::route('tags.show', $tag) }}">
                                <span class="tag">
                                    {{ $tag->name }}
                                </span>
                            </a>
                        @endforeach
                        <br><br><br>
                        @if($lesson->resources)
                        <p class="wp-text sm-margin"><span class="icon-img ii-tag"></span> Resources</p>
                        @foreach($lesson->resources as $resource)
                            <a class="tag-cnt" href="{{ $resource->url }}" target="_blank">
                                <span class="tag">
                                    {{ $resource->title }}
                                </span>
                            </a>
                        @endforeach
                        <br><br><br>
                        @endif
                        <!-- <p class="wp-text"><span class="icon-img ii-arrow-ur"></span> <a href="javascript:;" id="share">TEST SHARE THIS STEP</a></p>-->
                         <p class="wp-text"><span class="icon-img ii-arrow-ur"></span> 
                         <a href="javascript:;" data-toggle="modal" data-target="#shareModal">SHARE THIS STEP</a></p>
                        <p class="wp-text">
                            <span class="icon-img ii-arrow-l"></span>
                            {!! Html::linkRoute('roadmaps.show', 'RETURN TO ROADMAP', [$roadmap, $lesson]) !!}
                        </p>
                        <br>
                        @if($lesson->ask_a_question_link)
                        <a href="{{$lesson->ask_a_question_link}}" target="_blank" class="btn btn-white">ASK A QUESTION <span class="icon-img ii-users"></span></a>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
<div id="shareModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Share this step</h4>
      </div>
      <form role="form" method="POST" action="{{URL::route('roadmaps.lessons.show', [$roadmap, $lesson])}}">
      <div class="modal-body">
            <div id="shareMessage" class=" text-center"></div>
          <div class="form-group">
            <label for="email">Email address:</label>
            <input type="email" class="form-control" id="email">
          </div>
          <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" name="name" class="form-control" id="name">
          </div>
          <div class="form-group">
            <label for="message">Message:</label>
            <textarea name="message"  id="message" class="form-control"></textarea>
          </div>          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="shareModalBtn">Share</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>

  </div>
</div>
@endsection
