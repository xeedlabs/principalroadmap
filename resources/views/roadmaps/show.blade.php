@extends('roadmaps.layout')

@section('body')
    <div class="body-cnt">
        <div class="container">
            <h1>Global leader roadmap</h1>
            {{-- dd($leaders['940']) --}}
            <p class="sb-main-title">{{ $roadmap->title }}</p>
            <p class="subtitle">{{ $roadmap->description }}</p>
            <br><br>
            {{-- IF USER, get the data --}}
            @if($user)
            <div class="perc-info-cnt clearfix">
                <div class="pi-user-pic">
                    <div class="c100 p{{ $percentage_completed }} big">
                        <span id="userImage">
                        @if($images)
                            @if($images->data->avatar)
                            <img alt="User Avatar" onerror="this.src='https://qa.viflearn.com/components/com_community/assets/user-Female.png';" src="{{env('JOOMLA_URL')}}/{{$images->data->avatar}}">
                            @else
                             <img alt="User Avatar" src="https://qa.viflearn.com/components/com_community/assets/user-Female.png">
                            @endif
                        @endif
                        </span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                </div>
                <span class="div-br"></span>
                <p class="pi-perc">{{ $percentage_completed }}%</p>
                <div class="pi-text">
                @if(intval($percentage_completed)< 25 )
                    <p class="red-txt">Let’s do this!</p>
                    <p class="pi-comment"> </p>
                @elseif(intval($percentage_completed)< 50)
                    <p class="red-txt">You’re on your way!</p>
                    <p class="pi-comment"> </p>
                @elseif(intval($percentage_completed)< 75)
                    <p class="red-txt">Way to go!</p>
                    <p class="pi-comment"> </p>
                @elseif(intval($percentage_completed)< 100)
                    <p class="red-txt">Almost there!</p>
                    <p class="pi-comment"> </p>
                @else
                    <p class="red-txt">You rocked it!</p>
                    <p class="pi-comment"> </p>
                @endif
                </div>
            </div>

            @endif
            <div class="div-line"></div>

            @foreach($roadmap->requiredModules as $module)
                @if($module->requiredLessons->count())
                    <div class="step-info-cont" id="module-{{ $module->slug }}">
                        <p class="sb-title">{{ $module->title }}</p>
                        @if(!$module->isCompletedByPrincipal())
                            <div class="row info-cnt">
                        @else
                            <div class="row info-cnt" style="display:none;">
                        @endif

                        <div class="col-sm-6">
                            <p class="top-info">{{ $module->description }}</p>
                            <div class="user-desc clearfix">
                                @if($module->joomla_user_id AND $leaders[$module->joomla_user_id])
                                <p class="ud-pic"><img src="{{env('JOOMLA_URL')}}/{{ $leaders[$module->joomla_user_id]['avatar'] }}" onerror="this.src='https://qa.viflearn.com/components/com_community/assets/user-Female.png';" alt=""></p>
                                <p class="ud-info">{{$leaders[$module->joomla_user_id]['name']}} is leading a discussion in this step.
                                    @if($module->discussion_link)
                                        <a href="{{$module->discussion_link}}" target="_blank">Join the discussion!</a></p>
                                    @endif
                                @endif
                            </div>
                        </div>

                        <div class="col-sm-6 {{ ($roadmap->force_modules_enabled || $roadmap->canTakeModule($module, $principal))? '': 'sic-disabled-info' }}">
                            <div class="sic-step-list">
                                <?php $i = 0 ?>
                                @foreach($module->requiredLessons as $lesson)
                                    <div class="ssl-item">
                                        <a href="{{ $roadmap->canTakeModule($module, $principal)? URL::route('roadmaps.lessons.show', [$roadmap, $lesson]) : URL::route('roadmaps.lessons.show', [$roadmap, $lesson]) }}">
                                            <span class="ssl-icon {{ $completed_lessons->contains($lesson)? 'ichecked': ($lesson->type == 'video'? 'istep-on': 'ipencil') }}"></span>
                                            @if(count($module->requiredLessons) != ++$i)
                                                <span class="ssl-connect-line"></span>
                                            @endif
                                            <div class="ssl-desc">
                                                <div class="clearfix ssl-bborder">
                                                    <p class="ssl-text">{{ $lesson->title }}</p>
                                                    <p class="ssl-time">{{ date('i:s', $lesson->ect * 60) }}</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                                @if(count($module->bonusLessons))
                                    <p class="sb-title">Bonus material</p>
                                    <div class="sic-step-list">
                                        <?php $i = 0 ?>
                                        @foreach($module->bonusLessons as $lesson)
                                            <div class="ssl-item">
                                                <a href="{{ URL::route('roadmaps.lessons.show', [$roadmap, $lesson]) }}">
                                                    <span class="ssl-icon {{ $completed_lessons->contains($lesson)? 'ichecked': 'istep-on' }}"></span>
                                                    @if(count($module->bonusLessons) != ++$i)
                                                        <span class="ssl-connect-line"></span>
                                                    @endif
                                                    <div class="ssl-desc">
                                                        <div class="clearfix ssl-bborder">
                                                            <p class="ssl-text">{{ $lesson->title }}</p>
                                                            <p class="ssl-time">{{ date('i:s', $lesson->ect * 60) }}</p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <p class="bottom-link">
                        @if(!$module->isCompletedByPrincipal())
                        <a role="button" class="close-step" onclick="return pr.roadmaps.lessonsToggle(this, 'module-{{ $module->slug}}');"><span></span> {{ count($module->completedRequiredLessonsByPrincipal()) }} of {{ count($module->requiredLessons) }} steps complete <span class="click-open" style="display:none"> - Click to open</span></a>
                        @else
                        <a role="button" class="open-step" onclick="return pr.roadmaps.lessonsToggle(this, 'module-{{ $module->slug}}');"><span></span> {{ count($module->completedRequiredLessonsByPrincipal()) }} of {{ count($module->requiredLessons) }} steps complete <span class="click-open"> - Click to open</span>
                        </a>
                        @endif
                    </p>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
@endsection
