@extends('roadmaps.layout')

@section('body')
    <div class="body-cnt">
        <div class="container">
            <h1>Roadmaps</h1>
            <div class="row">
                <div class="col-sm-8">

                    @foreach($roadmaps as $roadmap)
                    <div class="main-info-cont">
                        <p class="main-title">{{ $roadmap->title }}</p>
                        <p class="text">{{ $roadmap->description }}</p>
                        <p><a href="/roadmaps/{{ $roadmap->slug }}" class="sb-text">Go to roadmap</a></p>
                        <div class="div-line"></div>
                    </div>
                    @endforeach
                </div>
                <div class="col-sm-4 main-right-cnt"></div>
            </div>
        </div>
    </div>

@endsection


