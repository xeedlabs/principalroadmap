@extends('layout')

@section('body')
    <div class="body-cnt">
        <div class="container">
            <h1>Resources</h1>
            <div class="row">
                <div class="col-sm-8">

                    @foreach($resources as $resource)
                    <div class="main-info-cont">
                        <p class="main-title">
                            <a href="{{ $resource->url }}" class="sb-text">{{ $resource->title }}</a>
                        </p>
                        <p class="text">{{ $resource->description }}</p>
                        <p>
                            View resource
                        </p>

                        <div class="div-line"></div>
                    </div>
                    @endforeach
                </div>
                <div class="col-sm-4 main-right-cnt"></div>
            </div>
        </div>
    </div>

@endsection