<div id="back-to-top" class="btn-back-to-top">
    <button class="btn btn-green" title="Back to Top"><i class="fa fa-caret-up"></i></button>
</div>
<footer>
    <div class="row container f-top-section">
        <div class="col-sm-6">
            <div class="f-top-left">
                <div class="ftl-img">
                    {!! Html::image('images/bulidlc2.png') !!}
                </div>
                <div class="progress-cnt">
                    <p>
                        <button type="button" class="btn btn-transparent">NEXT</button>
                    </p>
                    <div class="progress">
                        <p class="progress-perc">0%</p>

                        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 row footer-links">
            <div class="col-sm-4">
                <p class="fl-title">VIF'S LEARNING CENTER</p>
                <a href="#">Professional Development</a>
                <a href="#">Project-Based Inquiry</a>
                <a href="#">Curriculum and Content</a>
                <a href="#">Digital Badging</a>
                <a href="#">Community</a>
                <a href="#">Theoretical Framework</a>
                <a href="#">Schedule a Demo</a>
            </div>
            <div class="col-sm-4">
                <p class="fl-title">GLOBAL GATEWAY</p>
                <a href="#">2015 End of Year Report</a>
                <a href="#">Friday Institute Effectiveness Study</a>
                <a href="#">Webinar Archive</a>
            </div>
            <div class="col-sm-4">
                <p class="fl-title">IMPLEMENTATION TOOLS</p>
                <a href="#">P21 K-12 Global Competence Indicators</a>
                <a href="#">P21 Global-Ready Teacher Competency Framework</a>
                <a href="#">Go Global Toolkit</a>
            </div>
        </div>
    </div>
    <div class="f-bottom-section">
        <div class="row container">
            <div class="col-xs-6 col-sm-3 copy-text-cnt">
                <p>All content &copy;2015 <br>VIF International Education
                    <br><br>
                    <a href="#">Terms of Service</a>
                </p>
            </div>
            <div class="col-xs-6 col-sm-3 footer-links social-links">
                <p class="fl-title">CONTACT US</p>
                <a href="#" class="sl-fb"></a>
                <a href="#" class="sl-tw"></a>
                <a href="#" class="sl-youtube"></a><br>
                <a href="#" class="sl-pin"></a>
                <a href="#" class="sl-in"></a>
                <a href="#" class="sl-gp"></a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <p class="fbs-img-cnt">
                    <a href="#">
                        {!! Html::image('images/ISTE-VIF-International-Education-SoA-seal.png') !!}
                    </a>
                </p>
            </div>
            <div class="col-xs-6 col-sm-3">
                <p class="fbs-img-cnt">
                    <a href="#">
                        {!! Html::image('images/IMScertified.png') !!}
                    </a>
                </p>
            </div>
        </div>
    </div>

</footer>

<script type="text/javascript" src="{{ asset('/js/app.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function () {

        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('header .header-cnt .bottom-menu').addClass('fixed-menu');
                $('#back-to-top').show();
            } else {
                $('header .header-cnt .bottom-menu').removeClass('fixed-menu');
                $('#back-to-top').hide();
            }
        });

        var wdwHeight = $(window).height();
        $('.main-menu-mobi').height(wdwHeight);

    });

    (function ($) {
        // Back to top
        $('#back-to-top').on('click', function () {
            $("html, body").animate({scrollTop: 0}, 500);
            return false;
        });
    })(jQuery);

</script>
