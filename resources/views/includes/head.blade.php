<meta charset="UTF-8">
<title>VIF PR</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- CSS -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/frontend.css') }}" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<script type="text/javascript">
    if (!pr) {
        var pr = {};
    }

    pr.ver = "0.0.1";
    pr.base_url = "{{ url() }}/";
    pr._token = '{{ csrf_token() }}';

    window.addEventListener("load", function() {
        $.ajaxSetup({
            data: {
                _token: pr._token
            }
        });
    });
</script>
