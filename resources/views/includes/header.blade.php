<div class="main-menu-mobi mobi-show">
    <div class="mmm-title-cnt">
        <p class="close-btn"><a rel="button" onclick="$('.main-cont-page').removeClass('move-body');$('.main-menu-mobi').removeClass('open-menu');$('#close-mmm').hide();$('#open-mmm').show();">x</a></p>
        <p>VIF MENU</p>
    </div>
    <div class="mmm-items">
        <ul class="mmm-top-menu">
            <li><a href="#">Our Blog</a></li>
            <li><a href="#">My Learning Center</a></li>
            <li><a href="#">Upgrade Now!</a></li>
            <li><a href="#">Log out</a></li>
        </ul>

        <ul>
            <li><a href="#">My Profile</a></li>
            <li><a href="#">My PD</a></li>
            <li><a href="#">My Lesson Plans</a></li>
            <li><a href="#">My Badges</a></li>
            <li><a href="#">Resource Library</a></li>
            <li><a href="#">Classroom Partnerships</a></li>
            <li><a href="#">Support</a></li>
        </ul>
    </div>
</div>

<header>
    <div class="header-cnt">
        <div class="row">
            <div class="col-xs-6 col-sm-4">
                <a href="#" class="logo-cnt">
                    {!! Html::image('images/logo-vl.png', 'Vifs Learning Center') !!}
                </a>
            </div>
            <div class="col-xs-6 col-sm-8 menu-cnt">
                <div class="main-menu clearfix">
                    <div class="mm-select-cnt">
                        <select class="lang-options">
                                <option class="english"> English</option>
                                <option> <span class="arabic"></span> Arabic</option>
                                <option> <span class="armenian"></span> Armenian</option>
                                <option> <span class="chinese"></span> Chinese</option>
                                <option> <span class="dutch"></span> Dutch</option>
                                <option> <span class="french"></span> French</option>
                                <option> <span class="german"></span> German</option>
                                <option> <span class="greek"></span> Greek</option>
                                <option> <span class="indi"></span> Indi</option>
                                <option> <span class="italian"></span> Italian</option>
                                <option> <span class="japanese"></span> Japanese</option>
                                <option> <span class="korean"></span> Korean</option>
                                <option> <span class="portuguese"></span> Portuguese</option>
                                <option> <span class="romanian"></span> Romanian</option>
                                <option> <span class="russian"></span> Russian</option>
                                <option class="spanish"> Spanish</option>
                        </select>
                    </div>

                    <ul class="main-menu-options mobi-hide">
                        <li class="current"><a href="#">MY LEARNING CENTER</a></li>
                        <li><a href="#">OUR BLOG</a></li>
                        <li><a href="#">UPGRADE NOW</a></li>
                        @if (Auth::user())
                            <li><a href="#">LOGOUT</a></li>
                        @endif
                    </ul>

                    <button id="open-mmm" class="btn btn-white mobi-show" type="button" onclick="$('.main-cont-page').addClass('move-body');$('#open-mmm').hide();$('#close-mmm').show();">
                        <i class="fa fa-bars"></i>
                    </button>
                    <button id="close-mmm" class="btn btn-white mobi-show" type="button" onclick="$('.main-cont-page').removeClass('move-body');$('#close-mmm').hide();$('#open-mmm').show();" style="display:none;">
                        <i class="fa fa-bars"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="bottom-menu clearfix">
            <ul class="clearfix mobi-menu mobi-show">
                <li><a href="#"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
                <li><a href="#" class="icon-notify"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span><span class="in-qant">1</span></a></li>
                <li><a href="#" class="icon-notify"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></a></li>
                <li><a href="#" class="icon-notify"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span></a></li>
                <li class="logout-item"><a href="#"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span></a></li>
            </ul>

            <ul class="clearfix bm-collapse-menu">
                <li class="option-icon"><a href="#"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle">My Profile</a>
                    <ul class="dropdown-menu">
                        <li class=""><a href="#">Change My Password</a></li>
                        <li class=""><a href="#">Customize My Page</a></li>
                        <li class=""><a href="#">Preferences</a></li>
                        <li class=""><a href="#">Edit Profile</a></li>
                        <li class=""><a href="#">Events</a></li>
                        <li class=""><a href="#">Groups</a></li>
                        <li class=""><a href="#">Photos</a></li>
                        <li class=""><a href="#">Friends</a></li>
                    </ul>
                </li>
                <li><a href="#">My PD</a></li>
                <li><a href="#">My Lesson Plans</a></li>
                <li><a href="#">My Badges</a></li>
                <li><a href="#">Resource Library</a></li>
                <li class="dropdown">
                    <a href="#">Classroom Partnerships</a>
                    <ul class="dropdown-menu">
                        <li class="active"><a href="#">Post a Project</a></li>
                        <li class=""><a href="#">Categories</a></li>
                        <li class=""><a href="#">My Projects</a></li>
                        <li class=""><a href="#">My Conversations</a></li>
                    </ul>
                </li>
                <li><a href="#">Support</a></li>
                <li class="option-icon"><a href="#" class="icon-notify"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span><span class="in-qant">1</span></a></li>
                <li class="option-icon"><a href="#" class="icon-notify"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></a></li>
                <li class="option-icon"><a href="#" class="icon-notify"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span></a></li>
                <li class="option-icon logout-item"><a href="#"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span></a></li>
            </ul>

            <a class="btn-navbar js-bar-collapse-btn mobi-show" id="showMenu" onclick="$('.bm-collapse-menu').addClass('open-menu');$('#showMenu').hide();$('#hideMenu').show();">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="btn-navbar js-bar-collapse-btn mobi-show" id="hideMenu" onclick="$('.bm-collapse-menu').removeClass('open-menu');$('#hideMenu').hide();$('#showMenu').show();" style="display:none;">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
        </div>
    </div>

</header>
