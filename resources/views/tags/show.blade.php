@extends('tags.layout')

@section('body')
    <div class="body-cnt">
        <div class="container">
            <h1>Tagged topics</h1>

            <div class="row">
                <div class="col-sm-8">
                    <p class="sb-main-title">{{ $tag->name }}</p>
                    <p class="text">{{ $tag->description }}</p>
                    <br><br>

                    @if ($tag->lessons->count())
                        <p class="main-title">Found in the following lessons</p>

                        @foreach($tag->lessons as $lesson)
                            <br/>
                            <br/>
                            <div class="main-info-cont">
                                <p class="text">
                                    <b>{{ $lesson->title}}:</b>
                                    @foreach($lesson->module->roadmaps as $roadmap)
                                        <a href="{{ URL::route('roadmaps.lessons.show', [$roadmap, $lesson]) }}" class="sb-text">{{ $roadmap->title }}</a>
                                    @endforeach
                                </p>
                            </div>
                        @endforeach
                    @endif

                </div>
                <div class="col-sm-4 main-right-cnt"></div>
            </div>

        </div>
    </div>
@endsection
