@extends('layout')

@section('body')
    <div class="body-cnt">
        <div class="container">
            <h1>Tags</h1>
            <div class="row">
                <div class="col-sm-8">

                    @foreach($tags as $tag)
                    <div class="main-info-cont">
                        <p class="main-title">{{ $tag->name }}</p>
                        <p class="text">{{ $tag->description }}</p>
                        <p>
                            <a href="{{ URL::route('tags.show', $tag) }}" class="sb-text">View Tag</a>
                        </p>

                        <div class="div-line"></div>
                    </div>
                    @endforeach
                </div>
                <div class="col-sm-4 main-right-cnt"></div>
            </div>
        </div>
    </div>

@endsection