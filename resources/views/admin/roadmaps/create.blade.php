@extends('admin.roadmaps.layout')

@section('body')

@include('admin.includes.sidebar')
    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h3>New roadmap</h3>
        <p></p>
        <br>
        @include('admin.roadmaps.form', ['route_name' => 'admin.roadmaps.store', 'method' => 'POST'])
    </section>
</div>
@endsection
