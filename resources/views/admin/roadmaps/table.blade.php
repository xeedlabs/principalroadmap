<table id="roadmaps" class="table table-bordered table-hover">
    <thead>
    <tr>
        <th>Title</th>
        <th>Link</th>
        <!--th>Owned by principal</th-->
        <th>In charge of admin</th>
        <th>Modules available</th>
        <th width="90">Action</th>
    </tr>
    </thead>
    <tbody>

    @forelse($roadmaps as $roadmap)
        <tr>
            <td>{!! Html::linkRoute('roadmaps.show', $roadmap->title, $roadmap, ['target' => '_blank']) !!}</td>
            <td>{{ URL::route('roadmaps.show', $roadmap) }}</td>
            <!--td><?php // $roadmap->principal->name ?></td-->
            <td>{{ $roadmap->admin->name }}</td>
            <td>{{ $roadmap->modules->count() }}</td>
            <td class="action">
                <a href="{{ URL::route('admin.roadmaps.modules.index', [$roadmap]) }}" title="Show roadmap details and modules">
                    <span class="fa fa-sitemap" aria-hidden="true"></span>
                </a>
                <a href="{{ URL::route('admin.roadmaps.edit', [$roadmap]) }}" title="Edit roadmap" class="pjax-requester">
                    <span class="fa  fa-edit" aria-hidden="true"></span>
                </a>
                {!! Form::open(['route' => ['admin.roadmaps.destroy', $roadmap] , 'method'=>'DELETE','class'=>'del-form', 'data-pjax' => '']) !!}
                {!! Form::submit('X', ['class' => 'btn-remove link', 'title' => 'Delete roadmap', 'aria-hidden' => 'true']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @empty
        <p class="text-center"><i>There are no roadmaps to show</i></p>
    @endforelse
    </tbody>
</table>

{!! $roadmaps->render(new App\Pagination\AdminLTETwoPresenter($roadmaps)) !!}
