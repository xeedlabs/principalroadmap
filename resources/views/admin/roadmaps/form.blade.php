@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{!! Form::model($roadmap, ['method' => $method, 'route' => [$route_name, $roadmap], 'data-pjax' => '']) !!}
<div id="" class="box box-primary collapse in">
    <div class="box-body">
        <div class="form-group">
            {!! Form::label('title') !!}
            {!! Form::text('title', null, ['placeholder' => trans('roadmap.fields.placeholders.title'), 'class' => 'form-control']) !!}
        </div>
        <!--div class="form-group">
            {!! Form::label('principal') !!}
            {!! Form::select('principal', $principals, $roadmap->principal? $roadmap->principal->uuid: null, [
                'placeholder' => 'Select a principal',
                'id' => 'principal',
                'class' => 'form-control'
            ]) !!}
        </div-->
        <div class="form-group">
            {!! Form::label('groups') !!}
            {!! Form::select('groups[]', $groups, $selected_groups, [
                'placeholder' => trans('roadmap.fields.placeholders.modules'),
                'id' => 'groups',
                'class' => 'form-control select2',
                'multiple'
            ]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description') !!}
            {!! Form::textarea('description', null, ['placeholder' => trans('roadmap.fields.placeholders.description'), 'class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('image_url') !!}
            {!! Form::text('image_url', null, ['placeholder' => trans('roadmap.fields.placeholders.image_url'), 'class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('modules') !!}
            {!! Form::select('modules[]', $modules, $selected_modules, [
                'placeholder' => trans('roadmap.fields.placeholders.modules'),
                'id' => 'modules',
                'class' => 'form-control select2',
                'multiple'
            ]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('force_modules_enabled', 'Force modules to be enabled always') !!}
            {!! Form::checkbox('force_modules_enabled', null, null) !!}
        </div>
    </div>
    <div class="box-footer text-right">
        {!! Html::linkRoute('admin.roadmaps.index', 'Back to list', [], ['class' => 'btn btn-default pjax-requester']) !!}
        {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
        @if ($route_name === 'admin.roadmaps.update')
            {!! Html::link('#', 'Delete', ['class' => 'btn btn-danger', 'data-toggle' => 'modal', 'data-target' => '#modal-remove']) !!}
        @endif
    </div>
</div>
{!! Form::close() !!}
