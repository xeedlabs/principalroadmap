<table id="modules" class="table table-bordered table-hover">
    <thead>
    <tr>
        <th>Title</th>
        <th>In charge of</th>
        <th>Image</th>
        <th>Order</th>
        <th>Is bonus content</th>
        <th width="65">Action</th>
    </tr>
    </thead>
    <tbody>

    @forelse($modules as $module)
        <tr>
            <td>{{ $module->title }}</td>
            <td>{{ $module->author->name }}</td>
            <td>{{ $module->image_url }}</td>
            <td>{{ $module->pivot->order }}</td>
            <td>{{ $module->pivot->bonus? 'Yes': 'No' }}</td>
            <td class="action">
                <a href="{{ URL::route('admin.roadmaps.modules.edit', [$roadmap, $module]) }}" title="Edit module" class="pjax-requester">
                    <span class="fa  fa-edit" aria-hidden="true"></span>
                </a>
                {!! Form::open(['route' => ['admin.roadmaps.modules.destroy', $roadmap, $module] ,'method'=>'DELETE','class'=>'del-form', 'data-pjax' => '']) !!}
                {!! Form::submit('X', ['class' => 'btn-remove link', 'title' => 'Delete module', 'aria-hidden' => 'true']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @empty
        <p class="text-center"><i>There are no modules set for this roadmap</i></p>
    @endforelse
    </tbody>
</table>

{!! $modules->render(new App\Pagination\AdminLTETwoPresenter($modules)) !!}
