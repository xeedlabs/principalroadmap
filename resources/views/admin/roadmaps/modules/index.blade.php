@extends('admin.roadmaps.modules.layout')

@section('body')

@include('admin.includes.sidebar')
    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h3>Modules for roadmap "{{ $roadmap->title }}"</h3>
        <p>{{ $roadmap->description }}</p>
        <br>

        <div class="box box-primary collapse in" >
            <div class="box-header">
                <div>
                    <a href="{{ URL::route('admin.roadmaps.index') }}" class="btn btn-app">
                        <i class="fa fa-mail-reply"></i> Back to list
                    </a>
                </div>
            </div>
            <div class="box-body">
                @include('admin.roadmaps.modules.table')
            </div>
        </div>
    </section>
</div>
@endsection
