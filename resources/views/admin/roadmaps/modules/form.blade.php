@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{!! Form::model($module_roadmap, ['method' => $method, 'route' => [$route_name, $module_roadmap->roadmap, $module_roadmap->module], 'data-pjax' => '']) !!}
<div id="" class="box box-primary collapse in">
    <div class="box-body">
        <div class="form-group">
            {!! Form::label('order') !!}
            {!! Form::selectRange('order', 1, 100, null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::checkbox('bonus', 1, null, ['class' => 'checkbox-inline']) !!}
            {!! Form::label('bonus', 'Bonus module (means optional)') !!}
        </div>
    </div>
    <div class="box-footer text-right">
        {!! Html::linkRoute('admin.roadmaps.modules.index', 'Back to list', [$module_roadmap->roadmap], ['class' => 'btn btn-default pjax-requester']) !!}
        {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
        @if ($route_name === 'admin.roadmaps.modules.update')
            {!! Html::link('#', 'Delete', ['class' => 'btn btn-danger pjax-requester', 'data-toggle' => 'modal', 'data-target' => '#modal-remove']) !!}
        @endif
    </div>
</div>
{!! Form::close() !!}
