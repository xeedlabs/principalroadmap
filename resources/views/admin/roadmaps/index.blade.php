@extends('admin.roadmaps.layout')

@section('body')

@include('admin.includes.sidebar')
    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h3>Roadmaps</h3>
        <p>.</p>
        <br>

        <div class="box box-primary collapse in" >
            <div class="box-header">
                <div>
                    <a href="{{ URL::route('admin.roadmaps.create') }}" class="btn btn-app pjax-requester">
                        <i class="fa fa-plus"></i> New roadmap
                    </a>
                </div>
            </div>
            <div class="box-body">
                @include('admin.roadmaps.table')
            </div>
        </div>
    </section>
</div>
@endsection
