<!doctype html>
<html>
<head>
    @include('admin.includes.head')
    @yield('head')
</head>
<body class="skin-blue fixed sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    <div id="header">
        @include('admin.includes.header')
    </div>

    <div id="pjax-container">
        @yield('body')
    </div>


    <div id="footer" class="footer">
        <div id="footer-main">
            @include('admin.includes.footer')
        </div>
        <div id="footer-specific">
            @yield('footer-specific')
        </div>
        <div id="footer-custom">
            @yield('footer')
        </div>
    </div>

</div>
<!-- ./wrapper -->
</body>
</html>
