@extends('admin.resources.layout')

@section('body')

@include('admin.includes.sidebar')
    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h3>New resource</h3>
        <p></p>
        <br>
        @include('admin.resources.form', ['route_name' => 'admin.resources.store', 'method' => 'POST'])
    </section>
</div>
@endsection
