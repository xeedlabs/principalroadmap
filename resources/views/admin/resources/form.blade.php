@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{!! Form::model($resource, ['method' => $method, 'route' => [$route_name, $resource], 'data-pjax' => '']) !!}
<div id="" class="box box-primary collapse in">
    <div class="box-body">
        <div class="form-group">
            {!! Form::label('title') !!}
            {!! Form::text('title', null, ['placeholder' => trans('resource.fields.placeholders.title'), 'class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('url') !!}
            {!! Form::text('url', null, ['placeholder' => trans('resource.fields.placeholders.url'), 'class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description') !!}
            {!! Form::textarea('description', null, ['placeholder' => trans('resource.fields.placeholders.description'), 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="box-footer text-right">
        {!! Html::linkRoute('admin.resources.index', 'Back to list', [], ['class' => 'btn btn-default pjax-requester']) !!}
        {!! Form::submit('Save', ['class' => 'btn btn-success', 'data-pjax' => '']) !!}
        @if ($route_name === 'admin.resources.update')
            {!! Html::link('#', 'Delete', ['class' => 'btn btn-danger pjax-requester', 'data-toggle' => 'modal', 'data-target' => '#modal-remove']) !!}
        @endif
    </div>
</div>
{!! Form::close() !!}
