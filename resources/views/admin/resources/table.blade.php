<table id="resources" class="table table-bordered table-hover">
    <thead>
    <tr>
        <th>Title</th>
        <th>URL</th>
        <th width="90">Action</th>
    </tr>
    </thead>
    <tbody>

    @forelse($resources as $resource)
        <tr>
            <td>{{ $resource->title }}</td>
            <td>{{ $resource->url }}</td>
            <td class="action">
                <a href="{{ URL::route('admin.resources.show', [$resource]) }}" title="Show resource details" class="pjax-requester">
                    <span class="fa fa-eye" aria-hidden="true"></span>
                </a>
                <a href="{{ URL::route('admin.resources.edit', [$resource]) }}" title="Edit resource" class="pjax-requester">
                    <span class="fa  fa-edit" aria-hidden="true"></span>
                </a>
                {!! Form::open(['route' => ['admin.resources.destroy', $resource] ,'method'=>'DELETE','class'=>'del-form', 'data-pjax' => '']) !!}
                {!! Form::submit('X', ['class' => 'btn-remove link', 'title' => 'Delete resource', 'aria-hidden' => 'true']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @empty
        <p class="text-center"><i>There are no resources to show</i></p>
    @endforelse
    </tbody>
</table>

{{-- $resources->render(new App\Pagination\AdminLTETwoPresenter($resources)) --}}
