@extends('admin.resources.layout')

@section('body')

@include('admin.includes.sidebar')
    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3>Resource</h3>
        <br>
        <div class="box box-primary collapse in" >
            <div class="box-header with-border">
                <h3 class="box-title">{{ $resource->title }}</h3>
            </div>
            <div >
                <!-- form start -->
                <div class="box-body">
                    <p class="text-left"><i>{{ $resource->url }}</i></p>
                    <p class="text-left"><i>{{ $resource->description }}</i></p>
                    {!! Html::linkRoute('admin.resources.index', 'Back to list', [], ['class' => 'btn btn-default pjax-requester']) !!}
                </div><!-- /.box-body -->
            </div>
        </div>
    </section>
</div>
@endsection
