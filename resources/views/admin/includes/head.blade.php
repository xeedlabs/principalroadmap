<meta charset="UTF-8">
<title>VIF PR</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- CSS -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
<link href="http://cc.xeedlabs.com/vendor/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
<!-- Select2 -->
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css">

<link href="{{ asset('css/AdminLTE.blue.min.css') }}" rel="stylesheet" type="text/css">

<link href="{{ asset('css/roadmap.css') }}" rel="stylesheet" type="text/css">


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

<script type="text/javascript">
    if (!admin) {
        var admin = {
            pr: {}
        };
    } else if (!admin.pr) {
        admin.pr = {};
    }

    admin.pr.ver = "0.0.1";
    admin.pr.base_url = "{{url()}}/";
    admin.pr._token = '{{ csrf_token() }}';
</script>
