<!-- jQuery + bootstrap-->
<script src="{{ asset('/js/app.js') }}"></script>

<!-- AdminLTE App -->
<script src="{{ asset('/js/AdminLTE.min.js') }}"></script>

<!-- AdminLTE Plugins -->
<script src="{{ asset('/js/jquery.slimscroll.min.js') }}"></script>

<!-- jQuery-pjax -->
<script src="{{ asset('/js/jquery.pjax.js') }}"></script>
