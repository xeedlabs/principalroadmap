<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <div class="user-panel">
        <div class="pull-left image">
          <img src="http://www.sessionlogs.com/media/icons/defaultIcon.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?=Auth::user()->first_name?> <?=Auth::user()->last_name?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="/admin/">
                    <i class="fa fa-home"></i> <span>Dashboard</span>
                </a>
            </li>
            @can('admin.roadmaps.index')
            <li class="treeview">
                <a href="{{ URL::route('admin.roadmaps.index') }}">
                    <i class="fa fa-map"></i> <span>Roadmaps</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ URL::route('admin.roadmaps.index') }}">
                            <i class="fa fa-list-alt"></i>List
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.roadmaps.create') }}">
                            <i class="fa fa-plus"></i>New
                        </a>
                    </li>
                </ul>
            </li>
            @endcan
            <li class="treeview">
                <a href="{{ URL::route('admin.modules.index') }}">
                    <i class="fa fa-clone"></i> <span>Modules</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ URL::route('admin.modules.index') }}">
                            <i class="fa fa-list-alt"></i>List
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.modules.create') }}">
                            <i class="fa fa-plus"></i>New
                        </a>
                    </li>
                </ul>
            </li>
            @can('admin.lessons.index')
            <li class="treeview">
                <a href="{{ URL::route('admin.lessons.index') }}">
                    <i class="fa fa-map"></i> <span>Lessons</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ URL::route('admin.lessons.index') }}">
                            <i class="fa fa-list-alt"></i>List
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.lessons.create') }}">
                            <i class="fa fa-plus"></i>New
                        </a>
                    </li>
                </ul>
            </li>
            @endcan
            <li class="treeview">
                <a href="{{ URL::route('admin.resources.index') }}">
                    <i class="fa fa-files-o"></i> <span>Resources</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ URL::route('admin.resources.index') }}">
                            <i class="fa fa-list-alt"></i>List
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.resources.create') }}">
                            <i class="fa fa-plus"></i>New
                        </a>
                    </li>
                </ul>
            </li>
            @can('admin.tags.index')
            <li class="treeview">
                <a href="{{ URL::route('admin.tags.index') }}">
                    <i class="fa fa-tags"></i> <span>Tags</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ URL::route('admin.tags.index') }}">
                            <i class="fa fa-list-alt"></i>List
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.tags.create') }}">
                            <i class="fa fa-plus"></i>New
                        </a>
                    </li>
                </ul>
            </li>
            @endcan
            <li>
                <a href="{{ env('JOOMLA_LOGIN') }}">
                    <i class="fa fa-sign-out"></i> <span>Logout</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
