@extends('admin.modules.layout')

@section('body')

@include('admin.includes.sidebar')
    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h3>New module</h3>
        <p></p>
        <br>
        @include('admin.modules.form', ['route_name' => 'admin.modules.store', 'method' => 'POST'])
    </section>
</div>
@endsection
