<script src="{{ asset('/js/select2.full.min.js') }}"></script>
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/admin.pr.modules.js') }}"></script>
<script>
    admin.pr.modules.init();
</script>
