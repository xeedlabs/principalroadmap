<table id="modules" class="table table-bordered table-hover">
    <thead>
    <tr>
        <th>Created</th>
        <th>Title</th>
        <th>In charge of</th>
        <th>Allowed to modify</th>
        
        <th>Lessons available</th>
        <th width="90">Action</th>
    </tr>
    </thead>
    <tbody>

    @forelse($modules as $module)
        <tr>
            <td>{{ $module->created_at }}</td>
            <td>{{ $module->title }}</td>
            <td>{{ $module->author->name }}</td>
            <td>{{ $module->authors->implode('name', ', ') }}</td>
            
            <td>{{ $module->lessons->count() }}</td>
            <td class="action">
                <a href="{{ URL::route('admin.modules.show', $module) }}" title="Show module details and lessons">
                    <span class="fa fa-sitemap" aria-hidden="true"></span>
                </a>
                @can('modify', $module)
                <a href="{{ URL::route('admin.modules.edit', $module) }}" title="Edit module" class="pjax-requester">
                    <span class="fa  fa-edit" aria-hidden="true"></span>
                </a>
                @endcan
                @can('destroy', $module)
                {!! Form::open(['route' => ['admin.modules.destroy', $module] ,'method'=>'DELETE','class'=>'del-form', 'data-pjax' => '']) !!}
                {!! Form::submit('X', ['class' => 'btn-remove link', 'title' => 'Delete module', 'aria-hidden' => 'true']) !!}
                {!! Form::close() !!}
                @endcan
            </td>
        </tr>
    @empty
        <p class="text-center"><i>There are no modules set for this roadmap</i></p>
    @endforelse
    </tbody>
</table>
<div> &nbsp; </div>
{{-- $modules->render(new App\Pagination\AdminLTETwoPresenter($modules)) --}}
