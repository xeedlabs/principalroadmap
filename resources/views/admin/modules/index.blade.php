@extends('admin.modules.layout')

@section('body')

@include('admin.includes.sidebar')
    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h3>Modules</h3>
        <p><small>Create, edit and delete roadmap's modules. Add and edit lessons within a module</small></p>
        <br>

        <div class="box box-primary collapse in" >
            <div class="box-header">
                <div>
                    <a href="{{ URL::route('admin.modules.create') }}" class="btn btn-app pjax-requester">
                        <i class="fa fa-plus"></i> New module
                    </a>
                </div>
            </div>
            <div class="box-body">
                @include('admin.modules.table')
            </div>
        </div>
    </section>
</div>
@endsection
