@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{!! Form::model($module, ['method' => $method, 'route' => [$route_name, $module], 'data-pjax' => '']) !!}
<div id="" class="box box-primary collapse in">
    <div class="box-body">
        @if(\Auth::user()->isAdmin())
            <div class="form-group">
                {!! Form::label('author', 'Author in charge') !!}
                {!! Form::select('author', $authors, $selected_author, [
                    'placeholder' => trans('module.fields.placeholder.author'),
                    'class' => 'form-control select2'
                ]) !!}
            </div>
        @endif
        <div class="form-group">
            {!! Form::label('authors', 'Authors that can modify') !!}
            {!! Form::select('authors[]', $authors, $selected_authors, [
                'placeholder' => trans('module.fields.placeholder.authors'),
                'id' => 'authors',
                'class' => 'form-control select2',
                'multiple' => '',
            ]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Leader', 'Leader for module') !!}
            {!! Form::select('joomla_user_id', $leaders, $selected_leader, [
                'placeholder' => trans('module.fields.placeholder.leader'),
                'id' => 'leader',
                'class' => 'form-control select2',
            ]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('discussion_link') !!}
            {!! Form::text('discussion_link', null, ['placeholder' => trans('module.fields.placeholders.discussion_link'), 'class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('title') !!}
            {!! Form::text('title', null, ['placeholder' => trans('module.fields.placeholders.title'), 'class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description') !!}
            {!! Form::textarea('description', null, ['placeholder' => trans('module.fields.placeholders.description'), 'class' => 'form-control']) !!}
        </div>
        
    </div>
    <div class="box-footer text-right">
        {!! Html::linkRoute('admin.modules.index', 'Back to list', [], ['class' => 'btn btn-default pjax-requester']) !!}
        {!! Form::submit('Save', ['class' => 'btn btn-success', 'data-pjax' => '']) !!}
        @if($route_name === 'admin.modules.update')
            @can('destroy', $module)
            {!! Html::link('#', 'Delete', ['class' => 'btn btn-danger pjax-requester', 'data-toggle' => 'modal', 'data-target' => '#modal-remove']) !!}
            @endcan
        @endif
    </div>
</div>
{!! Form::close() !!}
