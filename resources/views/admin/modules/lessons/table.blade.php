<table id="lessons" class="table table-bordered table-hover">
    <thead>
    <tr>
        <th>Title</th>
        <th>In charge of</th>
        <th>Image</th>
        <th>Type</th>
        <th>Tags</th>
        <th>Number of resources</th>
        <th>Order</th>
        <th>Is bonus content</th>
        <th width="90">Action</th>
    </tr>
    </thead>
    <tbody>

    @forelse($lessons as $lesson)
        <tr>
            <td>{{ $lesson->title }}</td>
            <td>{{ $lesson->author->name }}</td>
            <td>{{ $lesson->image_url }}</td>
            <td>{{ ucfirst($lesson->type) }}</td>
            <td>
                <b>{{ $lesson->tags->count() }}</b>:
                {{ $lesson->getTagsNamesText() }}</td>
            <td>{{ $lesson->resources->count() }}</td>
            <td>{{ $lesson->order }}</td>
            <td>{{ $lesson->bonus? 'Yes': 'No' }}</td>
            <td class="action">
                <a href="{{ URL::route('admin.modules.lessons.show', [$module, $lesson]) }}" title="Show lesson details" class="pjax-requester">
                    <span class="fa fa-eye" aria-hidden="true"></span>
                </a>
                @can('modify', $lesson)
                <a href="{{ URL::route('admin.modules.lessons.edit', [$module, $lesson]) }}" title="Edit lesson" class="pjax-requester">
                    <span class="fa  fa-edit" aria-hidden="true"></span>
                </a>
                @endcan
                @can('destroy', $lesson)
                {!! Form::open(['route' => ['admin.modules.lessons.destroy', $module, $lesson] ,'method'=>'DELETE','class'=>'del-form', 'data-pjax' => '']) !!}
                {!! Form::submit('X', ['class' => 'btn-remove link', 'title' => 'Delete lesson', 'aria-hidden' => 'true']) !!}
                {!! Form::close() !!}
                @endcan
            </td>
        </tr>
    @empty
        <p class="text-center"><i>There are no lessons set for this module</i></p>
    @endforelse
    </tbody>
</table>

{{-- $lessons->render(new App\Pagination\AdminLTETwoPresenter($lessons)) --}}
