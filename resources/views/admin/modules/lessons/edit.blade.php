@extends('admin.modules.lessons.layout')
@section('head')
    @include('admin.modules.lessons.includes.head')
@endsection

@section('body')

@include('admin.includes.sidebar')
    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3>Edit lesson</h3>
        <p></p>
        <br>
        @include('admin.modules.lessons.form', ['route_name' => 'admin.modules.lessons.update', 'method' => 'PUT'])
    </section>

    <div class="modal fade modal-dialog-center" id="modal-remove" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content-wrap">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">
                            By deleting this lesson, every record of every
                            principal that completed it, will be deleted as well.
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete this lesson?</p>
                    </div>
                    <div class="modal-footer">
                        {!! Form::open(['route' => ['admin.modules.lessons.destroy', $module, $lesson] ,'method'=>'DELETE', 'data-pjax' => '']) !!}
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-danger">Yes</button>{!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
