@extends('admin.modules.lessons.layout')

@section('body')

@include('admin.includes.sidebar')
    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h3>Lessons for module "{{ $module->title }}"</h3>
        <p>{{ $module->description }}</p>
        <br>

        <div class="box box-primary collapse in" >
            <div class="box-header">
                <div>
                    <a href="{{ URL::route('admin.modules.index') }}" class="btn btn-app">
                        <i class="fa fa-mail-reply"></i> Back to list
                    </a>
                    @can('modify', $module)
                    <a href="{{ URL::route('admin.modules.lessons.create', $module) }}" class="btn btn-app pjax-requester">
                        <i class="fa fa-plus"></i> New lesson
                    </a>
                    @endcan
                </div>
            </div>
            <div class="box-body">
                @include('admin.modules.lessons.table')
            </div>
        </div>
    </section>
</div>
@endsection
