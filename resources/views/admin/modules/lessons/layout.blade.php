@extends('admin.layout')
@section('head')
    @include('admin.lessons.includes.head')
@endsection
@section('footer-specific')
    @include('admin.modules.lessons.includes.footer')
@endsection
