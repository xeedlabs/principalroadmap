@extends('admin.modules.lessons.layout')
@section('head')
    @include('admin.modules.lessons.includes.head')
@endsection

@section('body')

@include('admin.includes.sidebar')
    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h3>New lesson</h3>
        <p></p>
        <br>
        @include('admin.modules.lessons.form', ['route_name' => 'admin.modules.lessons.store', 'method' => 'POST'])
    </section>
</div>
@endsection
