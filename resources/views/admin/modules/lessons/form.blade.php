@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<style>
    .bar {
        height: 18px;
        background: green;
    }
</style>

{!! Form::model($lesson, ['method' => $method, 'route' => [$route_name, $module, $lesson], 'data-pjax' => '']) !!}
<div id="" class="box box-primary collapse in">
    <div class="box-body">
        @if(\Auth::user()->isAdmin())
            <div class="form-group">
                {!! Form::label('author', 'Author in charge') !!}
                {!! Form::select('author', $authors, $selected_author, [
                    'placeholder' => trans('lesson.fields.placeholders.author'),
                    'class' => 'form-control select2'
                ]) !!}
            </div>
        @endif
        <div class="form-group">
            {!! Form::label('title') !!}
            {!! Form::text('title', null, ['placeholder' => trans('lesson.fields.placeholders.title'), 'class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description') !!}
            {!! Form::textarea('description', null, ['placeholder' => trans('lesson.fields.placeholders.description'), 'class' => 'form-control']) !!}
        </div>
{{--    <div class="form-group">
            {!! Form::label('image_url') !!}
            {!! Form::text('image_url', null, ['placeholder' => trans('lesson.fields.placeholders.image_url'), 'class' => 'form-control']) !!}
        </div> --}}
        <div class="form-group">
            {!! Form::label('order') !!}
            <!--{!! Form::selectRange('order', 1, 100, null, ['class' => 'form-control']) !!}-->
            {!! Form::text('order', null, ['placeholder' => '1', 'class' => 'form-control']) !!}

        </div>
        <div class="form-group">
            {!! Form::checkbox('bonus', 1, null, ['class' => 'checkbox-inline']) !!}
            {!! Form::label('bonus', 'Bonus lesson (means optional)') !!}
        </div>
        <div class="form-group">
            {!! Form::label('content') !!}
            {!! Form::textarea('content', null, ['placeholder' => trans('lesson.fields.placeholders.content'), 'class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('type') !!}
            {!! Form::select('type', $types, $selected_type, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('ect', 'Estimated completion time (in minutes)') !!}
            <!--{!! Form::selectRange('ect', 1, 1000, null, ['class' => 'form-control']) !!}-->
            {!! Form::text('ect', null, ['placeholder' => '1', 'class' => 'form-control']) !!}

        </div>
            <div class="form-group">
                {!! Form::label('Link for "Ask a Question" button') !!}
                {!! Form::text('ask_a_question_link', null, ['placeholder' => trans('lesson.fields.placeholders.ask_a_question_link'), 'class' => 'form-control']) !!}
            </div>
        <div class="form-group">
            {!! Form::label('tags') !!}
            {!! Form::select('tags[]', $tags, $selected_tags, [
                'placeholder' => trans('lesson.fields.placeholder.tags'),
                'id' => 'tags',
                'class' => 'form-control select2',
                'multiple' => ''
            ]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Resources') !!}
            {!! Form::select('resources[]', $resources, $selected_resources, [
                'placeholder' => trans('lesson.fields.placeholder.resources'),
                'id' => 'resources',
                'class' => 'form-control select2',
                'multiple' => ''
            ]) !!}
        </div>
    </div>

    <div class="box-footer text-right">
        {!! Html::linkRoute('admin.modules.lessons.index', 'Back to list', [$module], ['class' => 'btn btn-default pjax-requester']) !!}
        {!! Form::submit('Save', ['class' => 'btn btn-success', 'data-pjax' => '']) !!}
        @if ($route_name === 'admin.modules.lessons.update')
            {!! Html::link('#', 'Delete', ['class' => 'btn btn-danger pjax-requester', 'data-toggle' => 'modal', 'data-target' => '#modal-remove']) !!}
        @endif
    </div>
</div>
{!! Form::close() !!}
{!! Form::open(['route' => 'admin.resources.upload','class'=>'upload-form']) !!}
    <div>
        <span class="btn btn-success fileinput-button">
            <i class="fa fa-plus"></i>
            <span>Select files...</span>
            {!! Form::file('resource-file', ['id' => 'fileupload', 'multiple' => '']) !!}
        </span>
    </div>
{!! Form::close() !!}

<div>
    &nbsp;
</div>
<div id="progress" class="progress hidden">
    <div class="progress-bar progress-bar-success"></div>
</div>

<div class="modal fade" id="mediaModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Select resource</h4>
            </div>
            <div class="modal-body">
                <div class="res">
                    <input type="checkbox" name="idFileX">
                    &nbsp;<span> Something.pdf</span>
                </div>
                <div class="res">
                    <input type="checkbox" name="idFileX">
                    &nbsp;<span> Something.pdf</span>
                </div>
                <div class="res">
                    <input type="checkbox" name="idFileX">
                    &nbsp;<span> Something.pdf</span>
                </div>
            </div>
            <div class="modal-body">
                <button type="button" class="btn btn-primary">Upload New Resource</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->
