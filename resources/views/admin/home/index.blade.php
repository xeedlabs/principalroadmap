@extends('admin.layout')

@section('body')

@include('admin.includes.sidebar')

    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Principal Road Dashboard
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::route('admin.home.index') }}"><i class="fa fa-dashboard"></i>Home</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- TIP -->


       {{-- <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">CPU Traffic</span>
                        <span class="info-box-number">90<small>%</small></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Likes</span>
                        <span class="info-box-number">41,410</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Sales</span>
                        <span class="info-box-number">760</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">New Members</span>
                        <span class="info-box-number">2,000</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div> --}}

        <h2 class="page-header">Roadmaps</h2>

        <div class="row">
            @forelse($roadmaps as $roadmap)
                <?php
                $total_lessons=0;
                $description=(strlen($roadmap->description) > 51)? substr($roadmap->description, 0, 50).'...' : $roadmap->description;
                ?>
                @foreach ($roadmap->modules as $module)
                    <?php $total_lessons += $module->lessons->count(); ?>
                @endforeach


                <div class="col-md-4">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-yellow" onclick="window.open('{{ URL::route('roadmaps.show', [$roadmap]) }}','_blank');">
                            <div class="widget-user-image">
                                <i class="fa fa-map-o"></i>
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">{{ $roadmap->title }}</h3>
                            <h5 class="widget-user-desc">{{ $description }}</h5>
                        </div>
                        <div class="box-footer no-padding">
                            <ul class="nav nav-stacked">
                                <li><p>Users who completed a roadmap <span class="pull-right badge bg-blue">{{ $roadmap->completedByPrincipals($principals)->count() }}</span></p></li>
                                <li><p>Users who started a roadmap <span class="pull-right badge bg-blue">{{ $roadmap->startedByPrincipals($principals)->count() }}</span></p></li>
                                <li><a href="{{ URL::route('admin.roadmaps.modules.index', [$roadmap]) }}" title="Show roadmap details and modules">Modules available<span class="pull-right badge bg-aqua">{{$roadmap->modules->count()}}</span></a></li>
                                <li><p>Lessons available <span class="pull-right badge bg-green">{{$total_lessons}}</span></p></li>
                                <li class="bf-btn-cnt"><a href="{{ URL::route('admin.roadmaps.edit', [$roadmap]) }}" title="Edit roadmap" class="btn btn-info btn-sm">Edit</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-user -->
                </div>
                <!-- /.col -->
            @empty
                <p class="text-center"><i>There are no roadmaps to show</i></p>
            @endforelse


        </div>




    </section><!-- /.content -->





</div><!-- /.content-wrapper -->

<footer class="main-footer clearfix">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.3.0
    </div>
</footer>



@endsection



