@extends('admin.tags.layout')

@section('body')

@include('admin.includes.sidebar')
    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3>Tag</h3>
        <br>
        <div class="box box-primary collapse in" >
            <div class="box-header with-border">
                <h3 class="box-title">{{ $tag->name }}</h3>
            </div>
            <div >
                <!-- form start -->
                <div class="box-body">
                    <p class="text-left"><i>{{ $tag->description }}</i></p>
                    {!! Html::linkRoute('admin.tags.index', 'Back to list', [], ['class' => 'btn btn-default pjax-requester']) !!}
                </div><!-- /.box-body -->
            </div>
        </div>
    </section>
</div>
@endsection
