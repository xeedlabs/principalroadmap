@extends('admin.tags.layout')

@section('body')

@include('admin.includes.sidebar')
    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h3>Tags</h3>
        <p>.</p>
        <br>

        <div class="box box-primary collapse in" >
            <div class="box-header">
                <div>
                    <a href="{{ URL::route('admin.tags.create') }}" class="btn btn-app pjax-requester">
                        <i class="fa fa-plus"></i> New tag
                    </a>
                </div>
            </div>
            <div class="box-body">
                @include('admin.tags.table')
            </div>
        </div>
    </section>
</div>
@endsection
