<table id="tags" class="table table-bordered table-hover">
    <thead>
    <tr>
        <th>Name</th>
        <th>Lessons tagged with</th>
        <th width="90">Action</th>
    </tr>
    </thead>
    <tbody>

    @can('admin.tags.edit')
        <?php $can_modify = true; ?>
    @endcan

    @forelse($tags as $tag)
        <tr>
            <td>{{ $tag->name }}</td>
            <td>{{ $tag->lessons->count() }}</td>
            <td class="action">
                <a href="{{ URL::route('admin.tags.show', [$tag]) }}" title="Show tag details" class="pjax-requester">
                    <span class="fa fa-eye" aria-hidden="true"></span>
                </a>
                @if(! empty($can_modify))
                    <a href="{{ URL::route('admin.tags.edit', [$tag]) }}" title="Edit tag" class="pjax-requester">
                        <span class="fa  fa-edit" aria-hidden="true"></span>
                    </a>
                    {!! Form::open(['route' => ['admin.tags.destroy', $tag] ,'method'=>'DELETE','class'=>'del-form', 'data-pjax' => '']) !!}
                    {!! Form::submit('X', ['class' => 'btn-remove link', 'title' => 'Delete tag', 'aria-hidden' => 'true']) !!}
                    {!! Form::close() !!}
                @endif
            </td>
        </tr>
    @empty
        <p class="text-center"><i>There are no tags to show</i></p>
    @endforelse
    </tbody>
</table>

{{-- $tags->render(new App\Pagination\AdminLTETwoPresenter($tags)) --}}
