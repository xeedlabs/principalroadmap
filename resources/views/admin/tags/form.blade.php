@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{!! Form::model($tag, ['method' => $method, 'route' => [$route_name, $tag], 'data-pjax' => '']) !!}
<div id="" class="box box-primary collapse in">
    <div class="box-body">
        <div class="form-group">
            {!! Form::label('name') !!}
            {!! Form::text('name', null, ['placeholder' => trans('tag.fields.placeholders.name'), 'class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description') !!}
            {!! Form::textarea('description', null, ['placeholder' => trans('tag.fields.placeholders.description'), 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="box-footer text-right">
        {!! Html::linkRoute('admin.tags.index', 'Back to list', [], ['class' => 'btn btn-default pjax-requester']) !!}
        {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
        @if ($route_name === 'admin.tags.update')
            {!! Html::link('#', 'Delete', ['class' => 'btn btn-danger pjax-requester', 'data-toggle' => 'modal', 'data-target' => '#modal-remove']) !!}
        @endif
    </div>
</div>
{!! Form::close() !!}
