@extends('admin.tags.layout')

@section('body')

@include('admin.includes.sidebar')
    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h3>New tag</h3>
        <p></p>
        <br>
        @include('admin.tags.form', ['route_name' => 'admin.tags.store', 'method' => 'POST'])
    </section>
</div>
@endsection
