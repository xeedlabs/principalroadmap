@extends('admin.lessons.layout')

@section('body')

@include('admin.includes.sidebar')
    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h3>Lessons</h3>
        <p>Add, edit and delete lessons</p>
        <br>

        <div class="box box-primary collapse in" >
            <div class="box-header">
                <div>
                    <a href="{{ URL::route('admin.lessons.create') }}" class="btn btn-app pjax-requester">
                        <i class="fa fa-plus"></i> New lesson
                    </a>
                </div>
            </div>
            <div class="box-body">
                @include('admin.lessons.table')
            </div>
        </div>
    </section>
</div>
@endsection
