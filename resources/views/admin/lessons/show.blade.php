@extends('admin.lessons.layout')


@section('body')

@include('admin.includes.sidebar')
    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3>Lesson</h3>
        <br>
        <div class="box box-primary collapse in" >
            <div class="box-header with-border">
                <h3 class="box-title">{{ $lesson->title }}</h3>
            </div>
            <div >
                <!-- form start -->
                <div class="box-body">
                    <p class="text-left"><i>{{ $lesson->description }}</i></p>
                    <div>
                        {!! $lesson->content !!}
                    </div>
                    {!! Html::linkRoute('admin.lessons.index', 'Back to list', [], ['class' => 'btn btn-default pjax-requester']) !!}
                </div><!-- /.box-body -->
            </div>
        </div>
    </section>
</div>
@endsection
