@extends('admin.lessons.layout')
@section('head')
    @include('admin.lessons.includes.head')
@endsection

@section('body')

@include('admin.includes.sidebar')
    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h3>New lesson</h3>
        <p></p>
        <br>
        @include('admin.lessons.form', ['route_name' => 'admin.lessons.store', 'method' => 'POST'])
    </section>
</div>
@endsection
