<!doctype html>
<html>
<head>
</head>
<body>
<div class="main-cont-page">
    Hi {{ $name }},

    {{ $user->name }} has shared a page in the VIF learning center with you!

    <p>
        <a href="{{ URL::route('roadmaps.lessons.show', [$roadmap, $lesson]) }}">{{ $lesson->title }}</a>
    </p>

    <p>
        Message from {{ $user->name }}: {{ $content }}</div>
    </p>
</body>
</html>
