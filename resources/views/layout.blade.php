<!doctype html>
<html>
<head>
    @include('includes.head')
    @yield('head')
</head>
<body>
<div class="main-cont-page">
    <div id="header">
        @include('includes.header')
    </div>

    <div id="pjax-container">
        @yield('body')
    </div>

    <div id="footer" class="footer">
        <div id="footer-main">
            @include('includes.footer')
        </div>
        <div id="footer-specific">
            @yield('footer-specific')
        </div>
        <div id="footer-custom">
            @yield('footer')
        </div>
    </div>
</div>
</body>
</html>
