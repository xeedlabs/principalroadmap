<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Models\Tag;
use App\Models\User;
use App\Models\Admin;
use App\Models\Author;
use App\Models\Lesson;
use App\Models\Module;
use App\Models\Roadmap;
use App\Models\Resource;
use App\Models\Principal;
use App\Models\ModuleRoadmap;

$factory->define(User::class, function (Faker\Generator $faker) {
    return [
        'uuid'           => Uuid::generate(),
        'first_name'     => $faker->firstName,
        'last_name'      => $faker->lastName,
        'email'          => $faker->unique()->email,
        'password'       => Hash::make(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Admin::class, function (Faker\Generator $faker) {
    return [
        'id' => factory(User::class)->create()->id
    ];
});

$factory->define(Author::class, function (Faker\Generator $faker) {
    return [
        'id' => factory(User::class)->create()->id
    ];
});

$factory->define(Principal::class, function (Faker\Generator $faker) {
    return [
        'id' => factory(User::class)->create()->id
    ];
});

$factory->define(Roadmap::class, function (Faker\Generator $faker) {
    return [
        'admin_id'     => Admin::orderBy(\DB::raw('RAND()'))->first()->id,
        'principal_id' => Principal::orderBy(\DB::raw('RAND()'))->first()->id,
        'image_url'    => $faker->imageUrl(),
        'title'        => $faker->realText($faker->numberBetween(10, 20)),
        'description'  => $faker->paragraph($faker->numberBetween(1, 3)),
    ];
});

$factory->define(Module::class, function (Faker\Generator $faker) {
    return [
        'author_id'   => Author::orderBy(\DB::raw('RAND()'))->first()->id,
        'image_url'   => $faker->imageUrl(),
        'title'       => $faker->realText($faker->numberBetween(10, 20)),
        'description' => $faker->paragraph($faker->numberBetween(1, 3)),
    ];
});

$factory->define(Lesson::class, function (Faker\Generator $faker) {
    return [
        'author_id'         => Author::orderBy(\DB::raw('RAND()'))->first()->id,
        'module_id'         => Module::orderBy(\DB::raw('RAND()'))->first()->id,
        'order'             => $faker->unique(true)->numberBetween(1, LessonsTableSeeder::SEEDED),
        'bonus'             => $faker->boolean(10),
        'image_url'         => $faker->imageUrl(),
        'title'             => $faker->realText($faker->numberBetween(10, 20)),
        'description'       => $faker->paragraph($faker->numberBetween(1, 3)),
        'content'           => $faker->paragraph($faker->numberBetween(10, 20), true),
        'ask_a_question_link' => $faker->url(),
        'type'              => array_rand(Lesson::types()),
        'ect'               => $faker->numberBetween(1, 20),
    ];
});

$factory->define(Resource::class, function (Faker\Generator $faker) {
    return [
        'title'       => $faker->realText($faker->numberBetween(10, 20)),
        'url'         => $faker->imageUrl(),
        'description' => $faker->paragraph($faker->numberBetween(1, 3)),
        'type'        => $faker->mimeType(),
    ];
});

$factory->define(Tag::class, function (Faker\Generator $faker) {
    return [
        'name'        => $faker->realText(10),
        'description' => $faker->paragraph($faker->numberBetween(1, 3)),
    ];
});

$factory->define(ModuleRoadmap::class, function (Faker\Generator $faker) {
    return [
        'order' => $faker->unique(true)->numberBetween(1, ModulesTableSeeder::SEEDED),
        'bonus' => $faker->boolean(10),
    ];
});
