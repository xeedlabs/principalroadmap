<?php

use App\Models\Role;
use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    protected $labels = [];

    protected $shared_labels = [
        'roadmaps.show'         => '',
        'roadmaps.lessons.show' => '',
        'tags.show'             => ''
    ];

    protected $author_labels = [
        'admin.home.index'              => '',
        'admin.modules.store'           => '',
        'admin.modules.index'           => '',
        'admin.modules.create'          => '',
        'admin.modules.update'          => '',
        'admin.modules.destroy'         => '',
        'admin.modules.show'            => '',
        'admin.modules.edit'            => '',
        'admin.modules.lessons.store'   => '',
        'admin.modules.lessons.index'   => '',
        'admin.modules.lessons.create'  => '',
        'admin.modules.lessons.update'  => '',
        'admin.modules.lessons.destroy' => '',
        'admin.modules.lessons.show'    => '',
        'admin.modules.lessons.edit'    => '',
        'admin.lessons.store'           => '',
        'admin.lessons.index'           => '',
        'admin.lessons.create'          => '',
        'admin.lessons.update'          => '',
        'admin.lessons.destroy'         => '',
        'admin.lessons.show'            => '',
        'admin.lessons.edit'            => '',
        'admin.resources.store'         => '',
        'admin.resources.index'         => '',
        'admin.resources.create'        => '',
        'admin.resources.update'        => '',
        'admin.resources.destroy'       => '',
        'admin.resources.show'          => '',
        'admin.resources.edit'          => '',
        'admin.resources.upload'        => '',
        'admin.tags.index'              => '',
        'admin.tags.show'               => '',
    ];

    protected $admin_labels = [
        'admin.roadmaps.index'           => '',
        'admin.roadmaps.show'            => '',
        'admin.roadmaps.store'           => '',
        'admin.roadmaps.create'          => '',
        'admin.roadmaps.update'          => '',
        'admin.roadmaps.destroy'         => '',
        'admin.roadmaps.edit'            => '',
        'admin.roadmaps.modules.index'   => '',
        'admin.roadmaps.modules.show'    => '',
        'admin.roadmaps.modules.store'   => '',
        'admin.roadmaps.modules.create'  => '',
        'admin.roadmaps.modules.update'  => '',
        'admin.roadmaps.modules.destroy' => '',
        'admin.roadmaps.modules.edit'    => '',
        'admin.tags.store'               => '',
        'admin.tags.create'              => '',
        'admin.tags.update'              => '',
        'admin.tags.destroy'             => '',
        'admin.tags.edit'                => '',
    ];

    protected $principal_labels = [];

    public function __construct()
    {
        $this->admin_labels     = array_merge($this->shared_labels, $this->admin_labels, $this->author_labels,
            $this->principal_labels);
        $this->author_labels    = array_merge($this->shared_labels, $this->author_labels);
        $this->principal_labels = array_merge($this->shared_labels, $this->principal_labels, [
            'roadmaps.lessons.complete' => ''
        ]);

        $this->labels = array_merge($this->admin_labels, $this->author_labels, $this->principal_labels);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);

        foreach ($this->labels as $name => $label) {
            if (null === ($permission = Permission::whereName($name)->first())) {
                $permission        = new Permission();
                $permission->name  = $name;
                $permission->label = $label;
                $permission->save();
            }
        }

        if (($role_admin = Role::whereName(Role::ADMIN_NAME)->first())) {
            $role_admin->setPermissions(array_keys($this->admin_labels));
        }

        if (($role_author = Role::whereName(Role::AUTHOR_NAME)->first())) {
            $role_author->setPermissions(array_keys($this->author_labels));
        }

        if (($role_principal = Role::whereName(Role::PRINCIPAL_NAME)->first())) {
            $role_principal->setPermissions(array_keys($this->principal_labels));
        }
    }
}
