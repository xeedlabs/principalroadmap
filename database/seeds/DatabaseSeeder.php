<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(PermissionsTableSeeder::class);
        $this->call(GroupsTableSeeder::class);

        if (App::environment() !== 'production') {
            $this->call(UsersTableSeeder::class);
            $this->call(RoadmapsTableSeeder::class);
        }

        Model::reguard();
    }
}
