<?php

use App\Models\User;
use App\Models\Group;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminsTableSeeder::class);
        $this->call(AuthorsTableSeeder::class);
        $this->call(PrincipalsTableSeeder::class);

        $users = User::get();
        foreach ($users as $user) {
            $groups = Group::orderBy(\DB::raw('RAND()'))->take(1)->get();
            $user->groups()->sync($groups);
        }
    }
}
