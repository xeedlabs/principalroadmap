<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;

class AdminsTableSeeder extends Seeder
{
    const SEEDED = 3;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(User::class)->make();
        $user->email = env('TEST_ADMIN_EMAIL', 'admin@user.com');
        $user->password = \Hash::make(env('TEST_ADMIN_PASSWORD', 'secret'));
        $user->save();
        $user->admin()->create([]);
        $user->addToRoles(Role::ADMIN_NAME);

        for ($i = 1; $i < self::SEEDED; $i++) {
            $model = factory(\App\Models\Admin::class)->create();
            $model->user->addToRoles(Role::ADMIN_NAME);
        }
    }
}
