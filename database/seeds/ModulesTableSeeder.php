<?php

use App\Models\Author;
use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{
    const SEEDED = 30;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < self::SEEDED; $i++) {
            $module = factory(App\Models\Module::class)->create();

            $author_ids = Author::where('id', '!=', $module->author_id)->orderBy(\DB::raw('RAND()'))->take(rand(0, AuthorsTableSeeder::SEEDED))->get()->pluck('id')->toArray();

            $module->authors()->sync($author_ids);
        }
    }
}
