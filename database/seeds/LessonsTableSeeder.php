<?php

use App\Models\Tag;
use App\Models\Resource;
use App\Models\Principal;
use Illuminate\Database\Seeder;

class LessonsTableSeeder extends Seeder
{
    const SEEDED = 50;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ModulesTableSeeder::class);
        $this->call(ResourcesTableSeeder::class);
        $this->call(TagsTableSeeder::class);

        for ($i = 0; $i < self::SEEDED; $i++) {
            $lesson = factory(App\Models\Lesson::class)->create();

            $principals = Principal::orderBy(\DB::raw('RAND()'))->take(rand(0, PrincipalsTableSeeder::SEEDED))->get();
            $lesson->setPrincipals($principals->pluck('id'));

            $resources = Resource::orderBy(\DB::raw('RAND()'))->take(rand(1, 5))->get();
            $lesson->setResources($resources->pluck('id'));

            $tags = Tag::orderBy(\DB::raw('RAND()'))->take(rand(0, 10))->get();
            $lesson->tags()->sync($tags);
        }
    }
}
