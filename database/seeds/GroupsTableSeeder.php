<?php

use App\Models\Group;
use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    const SEEDED = 5;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Group::create([
            'id' => Group::AUTHORS,
            'name' => Group::name(Group::AUTHORS),
        ]);
        Group::create([
            'id' => Group::ADMINS_AUTHORS,
            'name' => Group::name(Group::ADMINS_AUTHORS),
        ]);
        Group::create([
            'id' => Group::PRINCIPALS_K_5,
            'name' => Group::name(Group::PRINCIPALS_K_5),
        ]);
        Group::create([
            'id' => Group::PRINCIPALS_6_12,
            'name' => Group::name(Group::PRINCIPALS_6_12),
        ]);
        Group::create([
            'id' => Group::PRINCIPALS_K_12,
            'name' => Group::name(Group::PRINCIPALS_K_12),
        ]);
    }
}
