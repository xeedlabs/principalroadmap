<?php

use App\Models\Lesson;
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    const SEEDED = 20;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < self::SEEDED; $i++) {
            factory(App\Models\Tag::class)->create();
        }
    }
}
