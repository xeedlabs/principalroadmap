<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;

class AuthorsTableSeeder extends Seeder
{
    const SEEDED = 5;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(User::class)->make();
        $user->email = env('TEST_AUTHOR_EMAIL', 'author@user.com');
        $user->password = \Hash::make(env('TEST_AUTHOR_PASSWORD', 'secret'));
        $user->save();
        $user->author()->create([]);
        $user->addToRoles(Role::AUTHOR_NAME);

        for ($i = 1; $i < self::SEEDED; $i++) {
            $author = factory(\App\Models\Author::class)->create();
            $author->user->addToRoles(Role::AUTHOR_NAME);
        }
    }
}
