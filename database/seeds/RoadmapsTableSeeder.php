<?php

use App\Models\Group;
use App\Models\Module;
use Illuminate\Database\Seeder;

class RoadmapsTableSeeder extends Seeder
{
    const SEEDED = 20;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LessonsTableSeeder::class);

        for ($i = 0; $i < self::SEEDED; $i++) {
            $roadmap = factory(App\Models\Roadmap::class)->create();

            $modules = Module::orderBy(\DB::raw('RAND()'))->take(rand(0,
                ModulesTableSeeder::SEEDED))->get();

            $groups = Group::orderBy(\DB::raw('RAND()'))->take(rand(0,
                GroupsTableSeeder::SEEDED - 1))->get();
            $roadmap->groups()->sync($groups);

            foreach ($modules as $module) {
                $module_roadmap = factory(App\Models\ModuleRoadmap::class)->make();
                $module_roadmap->roadmap_id = $roadmap->id;
                $module_roadmap->module_id = $module->id;
                $module_roadmap->save();
            }
        }
    }
}
