<?php

use App\Models\Lesson;
use Illuminate\Database\Seeder;

class ResourcesTableSeeder extends Seeder
{
    const SEEDED = 100;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < self::SEEDED; $i++) {
            factory(App\Models\Resource::class)->create();
        }
    }
}
