<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;

class AddPermissionsForLessons extends Migration
{
    protected $labels = [];

    protected $shared_labels = [
    ];

    protected $author_labels = [
        'admin.lessons.store'   => '',
        'admin.lessons.index'   => '',
        'admin.lessons.create'  => '',
        'admin.lessons.update'  => '',
        'admin.lessons.destroy' => '',
        'admin.lessons.show'    => '',
        'admin.lessons.edit'    => '',
    ];

    protected $admin_labels = [
    ];

    protected $principal_labels = [];

    public function __construct()
    {
        $this->author_labels = array_merge($this->shared_labels, $this->author_labels);

        $this->labels = array_merge($this->admin_labels, $this->author_labels, $this->principal_labels);
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            if (count($this->labels) != DB::table('permissions')->whereIn('name', array_keys($this->labels))->count()) {
                $role_ids = [];
                if (null !== ($role_admin = DB::table('roles')->whereName('admin')->first())) {
                    $role_ids[] = $role_admin->id;
                }

                if (null !== ($role_author = DB::table('roles')->whereName('author')->first())) {
                    $role_ids[] = $role_author->id;
                }

                $created_at = Carbon::now();
                $updated_at = $created_at;

                foreach ($this->labels as $name => $label) {
                    $raw_permission = [
                        'name'       => $name,
                        'label'      => $label,
                        'created_at' => $created_at,
                        'updated_at' => $updated_at,
                    ];

                    $permission_id = DB::table('permissions')->insertGetId($raw_permission);

                    foreach ($role_ids as $role_id) {
                        DB::table('permission_role')->insert([
                            'permission_id' => $permission_id,
                            'role_id'       => $role_id,
                            'created_at'    => $created_at,
                            'updated_at'    => $updated_at,
                        ]);
                    }
                }
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function() {
            foreach ($this->author_labels as $name => $label) {
                DB::table('permissions')->whereName($name)->delete();
            }
        });
    }
}
