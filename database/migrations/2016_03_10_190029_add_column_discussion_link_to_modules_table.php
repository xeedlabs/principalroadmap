<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDiscussionLinkToModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('modules', function(Blueprint $table) {
                $table->string('discussion_link')->nullable()->after('joomla_user_id');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('modules', function(Blueprint $table) {
                $table->dropColumn('discussion_link');
            });
        });
    }
}
