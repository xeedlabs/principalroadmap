<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAskAQuestionLinkToLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('lessons', function(Blueprint $table) {
                $table->string('ask_a_question_link')->nullable()->after('content');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('lessons', function(Blueprint $table) {
                $table->dropColumn('ask_a_question_link');
            });
        });
    }
}
