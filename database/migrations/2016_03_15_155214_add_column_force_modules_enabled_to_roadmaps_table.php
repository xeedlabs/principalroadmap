<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnForceModulesEnabledToRoadmapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('roadmaps', function (Blueprint $table) {
                $table->boolean('force_modules_enabled')->default(false)->after('description');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('roadmaps', function (Blueprint $table) {
                $table->dropColumn('force_modules_enabled');
            });
        });
    }
}
