<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleRoadmapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::create('module_roadmap', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('module_id')->unsigned();
                $table->foreign('module_id')->references('id')->on('modules')->onUpdate('cascade')->onDelete('cascade');
                $table->integer('roadmap_id')->unsigned();
                $table->foreign('roadmap_id')->references('id')->on('roadmaps')->onUpdate('cascade')->onDelete('cascade');
                $table->smallInteger('order')->unsigned()->defaults(1);
                $table->boolean('bonus')->defaults(false);
                $table->timestamps();
                $table->unique(['module_id', 'roadmap_id']);
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::drop('module_roadmap');
        });
    }
}
