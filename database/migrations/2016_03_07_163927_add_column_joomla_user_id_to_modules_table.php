<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnJoomlaUserIdToModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('modules', function(Blueprint $table) {
                $table->string('joomla_user_id')->nullable()->after('slug');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('modules', function(Blueprint $table) {
                $table->dropColumn('joomla_user_id');
            });
        });
    }
}
