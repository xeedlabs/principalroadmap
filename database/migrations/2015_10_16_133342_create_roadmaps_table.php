<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoadmapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::create('roadmaps', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('admin_id')->unsigned();
                $table->foreign('admin_id')->references('id')->on('admins')->onUpdate('cascade')->onDelete('cascade');
                $table->integer('principal_id')->unsigned()->nullable();
                $table->foreign('principal_id')->references('id')->on('principals')->onUpdate('cascade')->onDelete('cascade');
                $table->string('slug')->unique();
                $table->string('image_url')->nullable();
                $table->string('title');
                $table->string('description')->nullable();
                $table->timestamps();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::drop('roadmaps');
        });
    }
}
