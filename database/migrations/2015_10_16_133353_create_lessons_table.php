<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::create('lessons', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('author_id')->unsigned();
                $table->foreign('author_id')->references('id')->on('authors')->onUpdate('cascade')->onDelete('cascade');
                $table->integer('module_id')->unsigned();
                $table->foreign('module_id')->references('id')->on('modules')->onUpdate('cascade')->onDelete('cascade');
                $table->string('slug')->unique();
                $table->smallInteger('order')->unsigned()->defaults(1);
                $table->boolean('bonus')->defaults(false);
                $table->string('image_url')->nullable();
                $table->string('title');
                $table->string('description')->nullable();
                $table->smallInteger('ect')->unsigned()->default(10);
                $table->string('type')->default('video');
                $table->text('content');
                $table->timestamps();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::drop('lessons');
        });
    }
}
