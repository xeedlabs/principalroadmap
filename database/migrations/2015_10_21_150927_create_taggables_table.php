<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaggablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::create('taggables', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('tag_id')->unsigned();
                $table->integer('taggable_id')->unsigned();
                $table->string('taggable_type');
                $table->string('name');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::drop('taggables');
        });
    }
}
