<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthorModuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::create('author_module', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('author_id')->unsigned();
                $table->foreign('author_id')->references('id')->on('authors')->onUpdate('cascade')->onDelete('cascade');
                $table->integer('module_id')->unsigned();
                $table->foreign('module_id')->references('id')->on('modules')->onUpdate('cascade')->onDelete('cascade');
                $table->timestamps();
                $table->unique(['author_id', 'module_id']);
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::drop('author_module');
        });
    }
}
